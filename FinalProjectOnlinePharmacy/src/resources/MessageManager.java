package resources;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessageManager {

	public static String getProperty(String key, String lang) {
		if(lang==null){
			lang = "en";
		}
		Locale locale = new Locale(lang);
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle(
				"resources/Bundle", locale);
		return resourceBundle.getString(key);
	}

}
