package resources;

import java.util.ResourceBundle;

public class PathsManager {
	private final static ResourceBundle resourceBundle = ResourceBundle
			.getBundle("resources/Paths");

	private PathsManager() {
	}

	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}

}
