package by.epam.leki.entity;

public class Medicine  extends AbstractEntity{


	private String name;
	private String actSubst;
	private String medForm;
	private float price;
	private boolean prescReq;
	

	public boolean getPrescReq() {
		return prescReq;
	}
	public void setPrescReq(boolean prescReq) {
		this.prescReq = prescReq;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getActSubst() {
		return actSubst;
	}
	public void setActSubst(String actSubst) {
		this.actSubst = actSubst;
	}
	public String getMedForm() {
		return medForm;
	}
	public void setMedForm(String medForm) {
		this.medForm = medForm;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
		@Override
	public String toString() {
		return "Medicine [name=" + name + ", actSubst=" + actSubst + ", medForm=" + medForm + ", price=" + price
				+ ", presc_req=" + prescReq + "]";
	}

	
	
}