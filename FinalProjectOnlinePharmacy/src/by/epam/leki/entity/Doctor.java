package by.epam.leki.entity;

public class Doctor extends User{
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Doctor [name=" + name + "]";
	}
	

}
