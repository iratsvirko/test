package by.epam.leki.entity;

public enum Role {
	PHARMASIST, PATIENT, DOCTOR, GUEST;
}
