package by.epam.leki.entity;

public class Prescription extends AbstractEntity {


	private String medName;
	private String patientFirstName;
	private String patientLastName;
	private String doctorName;
	private int quantity;
	private String date;
	

	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getMedName() {
		return medName;
	}
	public void setMedName(String medName) {
		this.medName = medName;
	}
	public String getPatientFirstName() {
		return patientFirstName;
	}
	public void setPatientFirstName(String patientFirstName) {
		this.patientFirstName = patientFirstName;
	}
	public String getPatientLastName() {
		return patientLastName;
	}
	public void setPatientLastName(String patientLastName) {
		this.patientLastName = patientLastName;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	@Override
	public String toString() {
		return "Prescription [medName=" + medName + ", patientFirstName=" + patientFirstName + ", patientLastName="
				+ patientLastName + ", doctorName=" + doctorName + ", dose=" + quantity + ", date=" + date + "]";
	}
	

}
