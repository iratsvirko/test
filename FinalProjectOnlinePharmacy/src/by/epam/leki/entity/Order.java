package by.epam.leki.entity;

public class Order  extends AbstractEntity{
	
	private String med;
	private String patientFirstName;
	private String patientLastName;
	private int quantity;
	private String datetime;
	

	public String getMed() {
		return med;
	}
	public void setMed(String med) {
		this.med = med;
	}
	public String getPatientFirstName() {
		return patientFirstName;
	}
	public void setPatientFirstName(String patientFirstName) {
		this.patientFirstName = patientFirstName;
	}
	public String getPatientLastName() {
		return patientLastName;
	}
	public void setPatientLastName(String patientLastName) {
		this.patientLastName = patientLastName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	@Override
	public String toString() {
		return "Order [med=" + med + ", patientFirstName=" + patientFirstName + ", patientLastName=" + patientLastName
				+ ", quantity=" + quantity + ", datetime=" + datetime + "]";
	}

	
	


}