package by.epam.leki.entity;

public class Request extends AbstractEntity  {
	
	private int prescNumber;
	private String comment;

	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getPrescNumber() {
		return prescNumber;
	}

	public void setPrescNumber(int prescNumber) {
		this.prescNumber = prescNumber;
	}

	@Override
	public String toString() {
		return "Request [prescNumber=" + prescNumber + ", comment=" + comment + "]";
	}



}
