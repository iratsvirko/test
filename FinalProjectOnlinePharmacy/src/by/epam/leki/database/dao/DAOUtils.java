package by.epam.leki.database.dao;


import java.sql.SQLException;
import java.sql.Statement;


import org.apache.log4j.Logger;

public class DAOUtils {

	private static final Logger Log = Logger.getLogger(DAOUtils.class);
	
	
	public static void closeStatement(Statement statement) {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			Log.error("statement was not closed", e);
		}
	}

}