package by.epam.leki.database.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import by.epam.leki.database.ConnectionPool;
import by.epam.leki.database.dao.DAOUtils;
import by.epam.leki.database.dao.interfaces.OrderDAO;
import by.epam.leki.entity.Order;

public class OrderDAOImpl implements OrderDAO {

	private static final Logger LOGGER = Logger.getLogger(OrderDAOImpl.class);
	
	private static final String SQL_QUERY_ADD_ORDER = "INSERT INTO `order` (med,patient,quantity) VALUES ((SELECT med_id FROM medicine WHERE med_name=?),(SELECT id FROM patient WHERE (patient.first_name=? and patient.last_name=?)),?)";

	public boolean addOrder(Order order) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_ADD_ORDER);
			statement.setString(1, order.getMed());
			statement.setString(2, order.getPatientFirstName());
			statement.setString(3, order.getPatientLastName());
			statement.setInt(4, order.getQuantity());
		
			statement.executeUpdate();
	
			DAOUtils.closeStatement(statement);

		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
			return false;
		} finally {
			
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return true;
	}
}
