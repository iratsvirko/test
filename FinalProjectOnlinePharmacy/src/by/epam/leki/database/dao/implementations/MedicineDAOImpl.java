package by.epam.leki.database.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.leki.database.ConnectionPool;
import by.epam.leki.database.dao.DAOUtils;
import by.epam.leki.database.dao.interfaces.MedicineDAO;
import by.epam.leki.entity.Medicine;


public class MedicineDAOImpl implements MedicineDAO {

	private static final Logger Log = Logger.getLogger(MedicineDAOImpl.class);
	private static final String SQL_QUERY_ADD_MEDICINES = "INSERT INTO medicine (med_name,med_act_subst,med_form,med_price,presc_req) VALUES(?,?,?,?,?)";
	private static final String SQL_QUERY_REMOVE_MEDICINES = "DELETE FROM medicine WHERE med_id=?";
	private static final String SQL_QUERY_GET_MEDICINE = "SELECT med_id,med_name,med_act_subst,med_form,med_price,presc_req FROM medicine WHERE med_id=?";
	private static final String SQL_QUERY_GET_MEDICINE_BY_NAME = "SELECT med_id,med_name,med_act_subst,med_form,med_price,presc_req FROM medicine WHERE med_name=?";
	private static final String SQL_QUERY_SELECT_ALL = "SELECT med_id, medicine.med_name,medicine.med_act_subst,medicine.med_form,medicine.med_price,medicine.presc_req FROM medicine order by medicine.med_name limit 10 offset ?";
	private static final String SQL_QUERY_SELECT_ALL_PRESC = "SELECT med_id, medicine.med_name,medicine.med_act_subst,medicine.med_form,medicine.med_price,medicine.presc_req FROM medicine where medicine.presc_req =1 order by medicine.med_name  ";
	private static final String SQL_QUERY_COUNT_ALL = "SELECT COUNT(medicine.med_id) FROM medicine";
	
	@Override
	public Medicine getMed(int id) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		Medicine med = new Medicine();
		try {

			statement = connection.prepareStatement(SQL_QUERY_GET_MEDICINE);
			statement.setInt(1, id);
			ResultSet resultSet = statement.executeQuery();
			setMedFields(med, resultSet);

		} catch (SQLException e) {
			Log.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return med;
	}

	
	
	@Override
	public Medicine getMedByName(String name) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		Medicine med = new Medicine();
		try {

			statement = connection.prepareStatement(SQL_QUERY_GET_MEDICINE_BY_NAME);
			statement.setString(1, name);
			ResultSet resultSet = statement.executeQuery();
			setMedFields(med, resultSet);

		} catch (SQLException e) {
			Log.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return med;
	}



	private void setMedFields(Medicine med, ResultSet resultSet) throws SQLException {
		while (resultSet.next()) {
			med.setId(resultSet.getInt(1));
			med.setName(resultSet.getString(2));
			med.setActSubst(resultSet.getString(3));
			med.setMedForm(resultSet.getString(4));
			med.setPrice(resultSet.getFloat(5));
			med.setPrescReq(resultSet.getBoolean(6));
		}
	}

	@Override
	public boolean addMed(Medicine med) {
	
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement addMedsStatement = null;
	
		try {
			addMedsStatement = connection.prepareStatement(SQL_QUERY_ADD_MEDICINES);
			addMedsStatement.setString(1, med.getName());
			addMedsStatement.setString(2, med.getActSubst());
			addMedsStatement.setString(3, med.getMedForm());
			addMedsStatement.setFloat(4, med.getPrice());
			addMedsStatement.setBoolean(5, med.getPrescReq());
			addMedsStatement.executeUpdate();
	
		} catch (SQLException e) {
			Log.error("dao exception", e);
			return false;
		} finally {
			DAOUtils.closeStatement(addMedsStatement);
			pool.close(connection);
		}
		return true;
	}

	@Override
	public boolean deleteMed(int id) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_REMOVE_MEDICINES);
			statement.setInt(1, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			Log.error("dao exception", e);
			return false;
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return true;
	}
	

	
	@Override
	public List<Medicine> getMedsCatal(int offset) {
		ConnectionPool pool = ConnectionPool.getInstance();;
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Medicine> list = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_SELECT_ALL);
			statement.setInt(1, offset);
			resultSet = statement.executeQuery();
			list = initCatalogue(resultSet);
		} catch (SQLException e) {
			Log.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return list ;
	}
	
	
	@Override
	public List<Medicine> getMedsCatalPresc() {
		ConnectionPool pool = ConnectionPool.getInstance();;
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Medicine> list = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_SELECT_ALL_PRESC);
			resultSet = statement.executeQuery();
			list = initCatalogue(resultSet);
		} catch (SQLException e) {
			Log.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return list ;
	}
	
	

	
	private List <Medicine> initCatalogue(ResultSet resultSet) throws SQLException {
		List <Medicine> list = new ArrayList<>();

		while (resultSet.next()) {
			Medicine meds = createMed(resultSet);
			list.add(meds);
		}
		return list;
	}
	
	
	private Medicine createMed(ResultSet resultSet) throws SQLException {

		Medicine med = new Medicine();
	
		med.setId(resultSet.getInt(1));
		med.setName(resultSet.getString(2));
		med.setActSubst(resultSet.getString(3));
		med.setMedForm(resultSet.getString(4));
		med.setPrice(resultSet.getFloat(5));
		med.setPrescReq(resultSet.getBoolean(6));
		
		return med;
	}

	@Override
	public int getNumberOfPages() {
		ConnectionPool pool = ConnectionPool.getInstance();;
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		int number = 0;

		try {
				statement = connection.prepareStatement(SQL_QUERY_COUNT_ALL);
				resultSet = statement.executeQuery();
				number = initNumber(resultSet);
			
		} catch (SQLException e) {
			Log.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return number;
	}

	private int initNumber(ResultSet resultSet) throws SQLException {
		int number = 0;
		while (resultSet.next()) {
			number = resultSet.getInt(1);
		}
		if (number % 10 != 0) {
			return number / 10 + 1;
		} else {
			return number / 10;
		}
	}


	
	
}
