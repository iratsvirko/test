package by.epam.leki.database.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import by.epam.leki.database.ConnectionPool;
import by.epam.leki.database.dao.DAOUtils;

import by.epam.leki.database.dao.interfaces.UserDAO;
import by.epam.leki.entity.Role;
import by.epam.leki.entity.User;

public class UserDAOImpl implements UserDAO {
	
	private static final String SQL_QUERY_GET_USER="SELECT user.user_id, user.role,user.email,user.password FROM user WHERE user.email = ?";
	private static final Logger LOGGER = Logger.getLogger(UserDAO.class);
	
	public User getUser(String email) {
					
			ConnectionPool pool = ConnectionPool.getInstance();
			Connection connection = pool.getConnection();
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			User user = new User();
			
			try {
				statement = connection.prepareStatement(SQL_QUERY_GET_USER);
				statement.setString(1, email);
				resultSet = statement.executeQuery();
				setUserFields(user, resultSet);
						
			}
			catch (SQLException e) {
				LOGGER.error("dao exception", e);
			} finally {
				DAOUtils.closeStatement(statement);
				pool.close(connection);
			}
			return user;
		}	
		
		
	
	private void setUserFields(User user, ResultSet resultSet)
			throws SQLException {
		while (resultSet.next()) {
			user.setId(resultSet.getInt(1));
			user.setRole(Role.valueOf(resultSet.getString(2).toUpperCase()));
			user.setEmail(resultSet.getString(3));
			user.setPassword(resultSet.getString(4));
		}
	}	
	
	
}
