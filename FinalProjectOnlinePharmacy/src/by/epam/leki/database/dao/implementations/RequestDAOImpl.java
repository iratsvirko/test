package by.epam.leki.database.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.leki.database.ConnectionPool;
import by.epam.leki.database.dao.DAOUtils;
import by.epam.leki.database.dao.interfaces.RequestDAO;
import by.epam.leki.entity.Request;

public class RequestDAOImpl implements RequestDAO {
	private static final Logger LOGGER = Logger.getLogger(OrderDAOImpl.class);
	private static final String SQL_QUERY_ADD_REQUEST = "INSERT INTO request (presc,comment ) VALUES (?,?)";
	private static final String SQL_QUERY_GET_REQUEST = "SELECT request.id,request.presc,request.`comment` FROM request where request.id = ?";
	private static final String SQL_QUERY_DELETE_REQUEST = "DELETE FROM request WHERE id=?";
	private static final String SQL_QUERY_SELECT_ALL = "SELECT id, presc, comment from request";
	private static final String SQL_QUERY_SELECT_ALL_BY_DOC = "SELECT request.id, request.presc, request.comment  from request JOIN prescription  on request.presc=prescription.presc_id where prescription.doctor=?";
	private static final String SQL_QUERY_GET_REQUEST_BY_PRESC = "SELECT request.id,request.presc,request.`comment` FROM request where request.presc = ?";

	
	
	@Override
	public Request getRequest(int id) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Request req = new Request();

		try {

			statement = connection.prepareStatement(SQL_QUERY_GET_REQUEST);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			setReqFields(req, resultSet);

		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return req;

	}
	
	@Override
	public Request getRequestByPresc (int id) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Request req = new Request();

		try {

			statement = connection.prepareStatement(SQL_QUERY_GET_REQUEST_BY_PRESC);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			setReqFields(req, resultSet);

		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return req;

	}
	

	private void setReqFields(Request req, ResultSet resultSet) throws SQLException {
		while (resultSet.next()) {
			req.setId(resultSet.getInt(1));
			req.setPrescNumber(resultSet.getInt(2));
			req.setComment(resultSet.getString(3));

		}
	}

	@Override
	public boolean addRequest(Request req) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_ADD_REQUEST);

			statement.setInt(1, req.getPrescNumber());
			statement.setString(2, req.getComment());

			statement.executeUpdate();
			DAOUtils.closeStatement(statement);

		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
			return false;
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return true;
	}

	@Override
	public boolean deleteRequest(int id) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_DELETE_REQUEST);
			statement.setInt(1, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
			return false;
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return true;
	}
	public List<Request> getRequestCatalDoc(int id)
	 {
		ConnectionPool pool = ConnectionPool.getInstance();;
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Request> list = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_SELECT_ALL_BY_DOC);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			list = initCatalogue(resultSet);
		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return list ;
	}

	
	
	
	@Override
	public List<Request> getRequestCatal() {
		ConnectionPool pool = ConnectionPool.getInstance();;
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Request> list = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_SELECT_ALL);
			resultSet = statement.executeQuery();
			list = initCatalogue(resultSet);
		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return list ;
	}

	
	private List <Request> initCatalogue(ResultSet resultSet) throws SQLException {
		List <Request> list = new ArrayList<>();

		while (resultSet.next()) {
			Request prescs = createReq(resultSet);
			list.add(prescs);
		}
		return list;
	}
	
	
	private Request createReq(ResultSet resultSet) throws SQLException {

		Request req = new Request();
		req.setId(resultSet.getInt(1));
		req.setPrescNumber(resultSet.getInt(2));
		req.setComment(resultSet.getString(3));

	return req;
	}


}
