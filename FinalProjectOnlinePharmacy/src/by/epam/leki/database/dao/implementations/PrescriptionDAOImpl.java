package by.epam.leki.database.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import org.apache.log4j.Logger;

import by.epam.leki.database.ConnectionPool;
import by.epam.leki.database.dao.DAOUtils;
import by.epam.leki.database.dao.interfaces.PrescriptionDAO;
import by.epam.leki.entity.Prescription;

public class PrescriptionDAOImpl implements PrescriptionDAO {

	private static final Logger LOGGER = Logger.getLogger(OrderDAOImpl.class);
	private static final String SQL_QUERY_CREATE_PRESCRIPTION = "INSERT INTO prescription (med,patient,doctor,quantity,valid_date) VALUES ((SELECT med_id FROM medicine WHERE med_name=?),(SELECT id FROM patient WHERE (patient.first_name=? and patient.last_name=?)),(SELECT doctor.id FROM doctor WHERE doctor.name =?),?,?)";
	private static final String SQL_QUERY_GET_PRESCRIPTION = "SELECT prescription.presc_id,medicine.med_name,patient.first_name, patient.last_name,doctor.name,  prescription.quantity, prescription.valid_date FROM prescription JOIN patient on prescription.patient=patient.id JOIN medicine on prescription.med=medicine.med_id JOIN doctor on prescription.doctor=doctor.id WHERE prescription.presc_id=?";
	private static final String SQL_QUERY_GET_PRESCRIPTION_BY_PATIENT_MED = "SELECT prescription.presc_id,medicine.med_name,patient.first_name, patient.last_name,doctor.name,  prescription.quantity, prescription.valid_date FROM prescription JOIN patient on prescription.patient=patient.id JOIN medicine on prescription.med=medicine.med_id JOIN doctor on prescription.doctor=doctor.id WHERE patient.id=? AND medicine.med_id=? ";
	private static final String SQL_QUERY_REMOVE_PRESCRIPTION = "DELETE FROM prescription WHERE presc_id=?";
	private static final String SQL_QUERY_UPDATE_PRESCRIPTION = "UPDATE prescription SET prescription.valid_date = ? WHERE prescription.presc_id=?";
	private static final String SQL_QUERY_SELECT_ALL = "SELECT prescription.presc_id,medicine.med_name,patient.first_name, patient.last_name,doctor.name,  prescription.quantity, prescription.valid_date FROM prescription JOIN patient on prescription.patient=patient.id JOIN medicine on prescription.med=medicine.med_id JOIN doctor on prescription.doctor=doctor.id";
	private static final String SQL_QUERY_SELECT_ALL_BY_ID = "SELECT prescription.presc_id,medicine.med_name,patient.first_name, patient.last_name,doctor.name,  prescription.quantity, prescription.valid_date FROM prescription JOIN patient on prescription.patient=patient.id JOIN medicine on prescription.med=medicine.med_id JOIN doctor on prescription.doctor=doctor.id WHERE patient.id = ?";
	private static final String SQL_QUERY_SELECT_ALL_DOC = "SELECT prescription.presc_id,medicine.med_name,patient.first_name, patient.last_name,doctor.name,  prescription.quantity, prescription.valid_date FROM prescription JOIN patient on prescription.patient=patient.id JOIN medicine on prescription.med=medicine.med_id JOIN doctor on prescription.doctor=doctor.id where doctor.id=? ";
	private static final String SQL_QUERY_GET_PRESCRIPTION_REQ= "SELECT prescription.presc_id,medicine.med_name,patient.first_name, patient.last_name,doctor.name,  prescription.quantity, prescription.valid_date FROM prescription JOIN patient on prescription.patient=patient.id JOIN medicine on prescription.med=medicine.med_id JOIN doctor on prescription.doctor=doctor.id JOIN request on prescription.presc_id=request.presc WHERE request.presc=? ";
	
	@Override
	public Prescription getPresc(int id) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Prescription presc = new Prescription();

		try {
			statement = connection.prepareStatement(SQL_QUERY_GET_PRESCRIPTION);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			setPrescFields(presc, resultSet);

		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return presc;
	}

	private void setPrescFields(Prescription presc, ResultSet resultSet) throws SQLException {
		while (resultSet.next()) {
			presc.setId(resultSet.getInt(1));
			presc.setMedName(resultSet.getString(2));
			presc.setPatientFirstName(resultSet.getString(3));
			presc.setPatientLastName(resultSet.getString(4));
			presc.setDoctorName(resultSet.getString(5));
			presc.setQuantity(resultSet.getInt(6));
			presc.setDate(resultSet.getDate(7).toString());
		}
	}

	
	@Override
	public boolean addPresc(Prescription prescription) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_CREATE_PRESCRIPTION);

			statement.setString(1, prescription.getMedName());
			statement.setString(2, prescription.getPatientFirstName());
			statement.setString(3, prescription.getPatientLastName());
			statement.setString(4, prescription.getDoctorName());
			statement.setInt(5, prescription.getQuantity());
			statement.setDate(6, convertStringDate(prescription.getDate()));

			statement.executeUpdate();
			DAOUtils.closeStatement(statement);

		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
			return false;
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return true;
	}

	

	@Override
	public boolean deletePresc(int id) {

		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_REMOVE_PRESCRIPTION);
			statement.setInt(1, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
			return false;
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return true;
	}

	
	private java.sql.Date parseDate(Long date) {
		java.sql.Date sqlDate = new java.sql.Date(date);

		return sqlDate;
	}
	
	private java.sql.Date convertStringDate(String stringDate){
	DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	Date date = null;
	try {
		date = format.parse(stringDate);
	} catch (ParseException e) {
		LOGGER.error("parse exception", e);
		}
	java.sql.Date sqlDate = new java.sql.Date(date.getTime());
	return sqlDate;
	}
	
	@Override
	public boolean updatePresc(int id,String date) {
		
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		
		
		try {
			
			statement = connection.prepareStatement(SQL_QUERY_UPDATE_PRESCRIPTION);
			statement.setDate(1, parseDate(convertStringDate(date).getTime()));
			statement.setInt(2, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
			return false;
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return true;
	}

	
	@Override
	public List<Prescription> getPrescsCatal() {
		ConnectionPool pool = ConnectionPool.getInstance();
		
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Prescription> list = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_SELECT_ALL);
			resultSet = statement.executeQuery();
			list = initCatalogue(resultSet);
		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return list;
	}


	@Override
	public List<Prescription> getPrescsCatalDoc(int docID) {
		ConnectionPool pool = ConnectionPool.getInstance();

		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Prescription> list = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_SELECT_ALL_DOC);
			statement.setInt(1, docID);
			resultSet = statement.executeQuery();
			list = initCatalogue(resultSet);
		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return list;
	}
	@Override
	public List<Prescription> getPrescsCatalByID(int id) {
		ConnectionPool pool = ConnectionPool.getInstance();
		
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Prescription> list = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_SELECT_ALL_BY_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			list = initCatalogue(resultSet);
		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return list;
	}

	@Override
	public Prescription getPrescByPatientMed(int patientID, int medID) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Prescription presc = new Prescription();

		try {
			statement = connection.prepareStatement(SQL_QUERY_GET_PRESCRIPTION_BY_PATIENT_MED);
			statement.setInt(1, patientID);
			statement.setInt(2, medID);
			resultSet = statement.executeQuery();
			setPrescFields(presc, resultSet);

		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return presc;

	}

	@Override
	public Prescription getPrescByReq(int reqID) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Prescription presc = new Prescription();

		try {
			statement = connection.prepareStatement(SQL_QUERY_GET_PRESCRIPTION_REQ);
			statement.setInt(1, reqID);
			resultSet = statement.executeQuery();
			setPrescFields(presc, resultSet);

		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return presc;

	}

	private List<Prescription> initCatalogue(ResultSet resultSet) throws SQLException {
		List<Prescription> list = new ArrayList<>();

		while (resultSet.next()) {
			Prescription prescs = createPresc(resultSet);
			list.add(prescs);
		}
		return list;
	}

	private Prescription createPresc(ResultSet resultSet) throws SQLException {

		Prescription presc = new Prescription();

		presc.setId(resultSet.getInt(1));
		presc.setMedName(resultSet.getString(2));
		presc.setPatientFirstName(resultSet.getString(3));
		presc.setPatientLastName(resultSet.getString(4));
		presc.setDoctorName(resultSet.getString(5));
		presc.setQuantity(resultSet.getInt(6));
		presc.setDate(resultSet.getDate(7).toString());
		return presc;
	}

}
