package by.epam.leki.database.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import by.epam.leki.database.dao.interfaces.PatientDAO;
import by.epam.leki.database.ConnectionPool;
import by.epam.leki.database.dao.DAOUtils;
import by.epam.leki.entity.Patient;
import by.epam.leki.entity.Role;


public class PatientDAOImpl implements PatientDAO {

	
	private static final Logger LOGGER = Logger.getLogger(PatientDAO.class);
	private static final String SQL_QUERY_ADD_USER = "INSERT INTO user (role, email, password) VALUES (?,?,?)";
	private static final String SQL_QUERY_ADD_PATIENT = "INSERT INTO patient (id,first_name,last_name,phone_number,card_number) VALUES ((select user_id from user  where email= ?), ?, ?, ?, ?)";
	private static final String SQL_QUERY_GET_PATIENT="SELECT user.user_id, user.role, patient.first_name,patient.last_name, patient.phone_number, patient.card_number FROM user INNER JOIN patient on user.user_id=patient.id WHERE user.email=?";
	private static final String SQL_QUERY_GET_PATIENT_BY_NAME="SELECT user.user_id, user.role, patient.first_name,patient.last_name, patient.phone_number, patient.card_number FROM user INNER JOIN patient on user.user_id=patient.id WHERE patient.first_name=? and patient.last_name=?";
	
	
	@Override
	public boolean addPatient(Patient patient) {
		
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(SQL_QUERY_ADD_USER);
			statement.setString(1, patient.getRole().toString().toLowerCase());
			statement.setString(2, patient.getEmail());
			statement.setString(3, patient.getPassword());
			statement.executeUpdate();
			DAOUtils.closeStatement(statement);
			

			statement = connection.prepareStatement(SQL_QUERY_ADD_PATIENT);

			statement.setString(1, patient.getEmail());
			statement.setString(2, patient.getFirstName());
			statement.setString(3, patient.getLastName());
			statement.setString(4, patient.getPhoneNumber());
			statement.setString(5, patient.getCardNumber());
			statement.executeUpdate();
			
			
		} catch (SQLException e) {
			LOGGER.error("dao exception", e);
			return false;
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return true;
	}

	
	@Override
	public Patient getPatientByName(String firstName, String lastName) {
		
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Patient patient = new Patient();
		
		try {
			statement = connection.prepareStatement(SQL_QUERY_GET_PATIENT_BY_NAME);
			statement.setString(1, firstName);
			statement.setString(2, lastName);
			resultSet = statement.executeQuery();
			setPatientFields(patient, resultSet);
					
		}
		catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return patient;
		
	}


	@Override
	public Patient getPatient(String email) {
		
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Patient patient = new Patient();
		
		try {
			statement = connection.prepareStatement(SQL_QUERY_GET_PATIENT);
			statement.setString(1, email);
			resultSet = statement.executeQuery();
			setPatientFields(patient, resultSet);
					
		}
		catch (SQLException e) {
			LOGGER.error("dao exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
		}
		return patient;
	}	
	
	/**
	 * Set the fields of entity Patient according to ResultSet
	 */
	private void setPatientFields(Patient patient, ResultSet resultSet)
			throws SQLException {
		while (resultSet.next()) {
			patient.setId(resultSet.getInt(1));
			patient.setRole(Role.valueOf(resultSet.getString(2).toUpperCase()));
			patient.setFirstName(resultSet.getString(3));
			patient.setLastName(resultSet.getString(4));
			patient.setPhoneNumber(resultSet.getString(5));
			patient.setCardNumber(resultSet.getString(6));
		}
	}	

}
