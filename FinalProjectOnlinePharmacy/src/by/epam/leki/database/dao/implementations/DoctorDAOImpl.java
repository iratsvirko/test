package by.epam.leki.database.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import by.epam.leki.database.ConnectionPool;
import by.epam.leki.database.dao.DAOUtils;
import by.epam.leki.database.dao.interfaces.DoctorDAO;
import by.epam.leki.entity.Doctor;
import by.epam.leki.entity.Role;

public class DoctorDAOImpl implements DoctorDAO {

	private static final Logger LOGGER = Logger.getLogger(DoctorDAO.class);
	private static final String SQL_QUIERY_GET_DOCTOR = "SELECT user.user_id, user.role, doctor.name From user INNER JOIN doctor on user.user_id=doctor.id WHERE id = ?";

	@Override
	public Doctor getDoctor(int id) {
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultset = null;
		Doctor doc = new Doctor();

		try {
			statement = connection.prepareStatement(SQL_QUIERY_GET_DOCTOR);
			statement.setInt(1, id);
			resultset = statement.executeQuery();
			
			while (resultset.next()){
				doc.setId(resultset.getInt(1));
				doc.setRole(Role.valueOf(resultset.getString(2).toUpperCase()));
				doc.setName(resultset.getString(3));
			}

		} catch (SQLException e) {
			LOGGER.error("DAO exception", e);
		} finally {
			DAOUtils.closeStatement(statement);
			pool.close(connection);
			
		}

		return doc;
	}

}
