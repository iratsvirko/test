package by.epam.leki.database.dao.interfaces;


import by.epam.leki.entity.Order;

public interface OrderDAO {
		
	/**
	 * Adds Order to DB
	 * @param order
	 * @return boolean
	 */
	boolean addOrder(Order order);

}
