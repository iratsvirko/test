package by.epam.leki.database.dao.interfaces;

import java.util.List;

import by.epam.leki.entity.Medicine;

public interface MedicineDAO {

	/**
	 Gets Medicine by Medicine's id
	 * @return Medicine
	 * @param id
	 */	
	
	Medicine getMed (int id);
	
	/**
	 *  Gets Medicine by Medicine's name
	 *  @return Medicine
	 *  @param name
	 */
	Medicine getMedByName (String name);
	
	/**
	 * Adds Medicine to DB
	 * @return boolean
	 */
	boolean addMed(Medicine med);

	/**
	 *Delete Medicine by Medicine's id
	 *@param id
	 * @return boolean
	 */
	boolean deleteMed(int id);
	
	/**
	 * Gets list of all available Medicines
	 * @return List
	 */
	public List<Medicine> getMedsCatal(int offset);

	/**
	 * Gets list of Medicines, which requires Prescription to make Order
	 * @return List
	 */
	public List<Medicine> getMedsCatalPresc();
	/**
	 * Returns number of pages in catalog
	 * @return number
	 */
	public int getNumberOfPages();
	
}
