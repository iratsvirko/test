package by.epam.leki.database.dao.interfaces;

import by.epam.leki.entity.User;

public interface UserDAO {
	
	/**
	 * Gets User from User's email
	 * @param email
	 * @return User
	 */
	User getUser (String email);
	
}
