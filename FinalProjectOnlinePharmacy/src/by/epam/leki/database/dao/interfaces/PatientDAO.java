package by.epam.leki.database.dao.interfaces;


import by.epam.leki.entity.Patient;

public interface PatientDAO {
	
	/**
	 * Gets Patient according to his firstName and lastName
	 * @param firstName
	 * @param lastName
	 * @return Patient
	 */
	Patient getPatientByName(String firstName, String lastName);
	
	/**
	 * Gets Patient by its email
	 * @param email
	 * @return Patient
	 */
	Patient getPatient(String email);
	
	/**
	 * Adds Patient to DB
	 * @param patient
	 * @return boolean
	 */
	boolean addPatient(Patient patient);
	
}
