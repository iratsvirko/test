package by.epam.leki.database.dao.interfaces;

import java.util.List;

import by.epam.leki.entity.Request;

public interface RequestDAO {
	
	/**
	 * Gets Request by Request's id
	 * @param id
	 * @return Request
	 */
	Request getRequest (int id); 
	
	/**
	 * Gets Request by id of Prescription, which Request contains
	 * @param id
	 * @return Request
	 */
	Request getRequestByPresc (int id);
	
	/**
	 * Adds Request to DB
	 * @param req
	 * @return boolean
	 */
	boolean addRequest (Request req);
	
	/**
	 * Delete Request from DB by Request's id
	 * @param id
	 * @return boolean
	 */
	boolean deleteRequest (int id);
	
	/**
	 * Gets list of all available Requests
	 * @return List
	 */
	public List<Request> getRequestCatal();
	
	/**
	 * Gets list of Requests by id of Doctor, which added this Prescriptions
	 * @param id
	 * @return List
	 */
	public List<Request> getRequestCatalDoc(int id);
	

	

}
