package by.epam.leki.database.dao.interfaces;

import java.util.List;

import by.epam.leki.entity.Prescription;


public interface PrescriptionDAO {
	
	/**
	 * Gets Prescription by Prescription's id
	 * @param id
	 * @return Prescription
	 */
	Prescription getPresc (int id);
	/**
	 * Gets Prescription which has both Patient's id and Medicine's id

	 * @param patientID
	 * @param medID
	 * @return Prescription
	 */
	Prescription getPrescByPatientMed (int patientID,int medID);

	/**
	 * Gets Prescription by its Request's id
	 * @param reqID
	 * @return Prescription
	 */
	Prescription getPrescByReq (int reqID);

	/**
	 * Adds Prescription to DB
	 * @param presc
	 * @return boolean
	 */
	boolean addPresc (Prescription presc);
	
	/**
	 * Delete Prescription by Prescription's id
	 * @param id
	 * @return boolean
	 */
	boolean deletePresc(int id);
	
	/**
	 * Updates Prescription changing its Valid date on input parameter String date
	 * @param id
	 * @param date
	 * @return boolean
	 */
	boolean updatePresc(int id, String date);

	/**
	 * Gets list of all available Prescriptions
	 * @return List
	 */
	public List<Prescription> getPrescsCatal();

	/**
	 * Gets list of Prescriptions for certain Patient by its id 
	 * @param patID
	 * @return List
	 */
	public List<Prescription> getPrescsCatalByID(int patID);

	/**
	 * Gets list of Prescriptions for certain Doctor by its id 
	 * @param docID
	 * @return List
	 */
	public List<Prescription> getPrescsCatalDoc(int docID);
}
