package by.epam.leki.database.dao.interfaces;

import by.epam.leki.entity.Doctor;

public interface DoctorDAO {
	
	/**
	 * Gets Doctor by Doctor's  id
	 * @param id
	 * @return Doctor
	 */
	Doctor getDoctor (int id);

}
