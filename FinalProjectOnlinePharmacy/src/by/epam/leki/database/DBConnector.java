package by.epam.leki.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

class DBConnector {

	private static final Logger Log = Logger.getLogger(DBConnector.class);
	private static final ResourceBundle CONFIG_BUNDLE = ResourceBundle.getBundle("resources/Configuration");
	private static final String URL = CONFIG_BUNDLE.getString("database-url");
	private static Properties properties = new Properties();
	static {

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			Log.error("exception in initializing driver", e);
			throw new ExceptionInInitializerError();
		}
		properties.setProperty("user", CONFIG_BUNDLE.getString("database-user"));
		properties.setProperty("password", CONFIG_BUNDLE.getString("database-password"));
		properties.setProperty("useUnicode", CONFIG_BUNDLE.getString("database-unicode"));
		properties.setProperty("characterEncoding", CONFIG_BUNDLE.getString("database-encoding"));
	}


	public Connection getConnection() throws SQLException {
		Connection connection = DriverManager.getConnection(URL, properties);
		return connection;
	}
}
