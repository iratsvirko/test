package by.epam.leki.tests;

import org.junit.Test;

import by.epam.leki.database.ConnectionPool;

public class GetConnectionTestTime {

	private static ConnectionPool connectionPool;

	/**
	 * Pool creation time test.
	 */
	@Test(timeout = 2000)
	public void ConnectionPoolCreationTest() {
		connectionPool = ConnectionPool.getInstance();
		connectionPool.getConnection();
	}
}