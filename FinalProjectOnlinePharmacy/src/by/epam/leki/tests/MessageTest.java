package by.epam.leki.tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import resources.MessageManager;

@RunWith(Parameterized.class)

public class MessageTest {

	private String message = null;
	private String key = null;

	public MessageTest(String message, String key) {
		this.message = message;
		this.key = key;
	}

	@Parameters
	public static Collection<Object[]> data() {
		List<Object[]> messageAndKey = Arrays.asList(new Object[][] {

				{ "Please, fill in all of the fields.", "message.fill_all" },
				{ "User with such email already exists.", "message.email_exists" },
				{ "This patient already has prescription for such medicine.", "message.prescexists" },
				{ "There is no such patient in database.", "message.nopatient" },
				{ "Check the valid date of the prescription.", "message.checkdate" },
				{ "Email or password is incorrect.", "message.check_login" },
				{ "Sorry! That's an error. Something is wrong.", "message.error" },
				{ "Sorry, this operation is unavailable", "message.unavailable" },
				{ "Please, check your prescription.", "message.check_prescription" },
				{ "Request is deleted.", "message.reqdeleted" }, { "Prescription is extended.", "message.reqextended" },
				{ "Order succeed.", "message.order_succeed" }, { "Request is added.", "message.reqadded" },
				{ "Operation failed.", "message.operationfailed" }, { "Medicine is deleted.", "message.meddeleted" },
				{ "Medicine is added.", "message.medadded" }, { "Prescription is deleted.", "message.prescdeleted" },
				{ "Prescription is added.", "message.prescadded" },
				{ "This medicine already exists.", "message.med_exists" },
				{ "Sorry,your session is over. Please, log in again.", "message.session_end" },
				{ "This prescription is still valid.", "message.stillvalid" },
				{ "You've already made a request for this prescription.", "message.reqexists" } });

		return messageAndKey;
	}

	@Test
	public void testMessage() {
		assertEquals(message, MessageManager.getProperty(key, "en"));
	}
}
