package by.epam.leki.tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import resources.PathsManager;

@RunWith(Parameterized.class)
public class PathTest {

	private String path = null;
	private String key = null;

	public PathTest(String path, String key) {
		this.path = path;
		this.key = key;
	}

	@Parameters
	public static Collection<Object[]> data() {
		List<Object[]> pathAndKey = Arrays.asList(new Object[][] {

				{ "/jsp/common/home.jsp", "common.home" }, { "/jsp/common/login.jsp", "common.login" },
				{ "/jsp/common/register.jsp", "common.register" }, { "/jsp/common/catalogue.jsp", "common.catalogue" },
				{ "/jsp/common/error.jsp", "common.error" },
				{ "/jsp/common/session_end_error.jsp", "common.session_error" },
				{ "/jsp/patient/home.jsp", "patient.home" },
				{ "/jsp/patient/prescriptions.jsp", "patient.prescriptions" },
				{ "/jsp/patient/order.jsp", "patient.order" }, { "/jsp/patient/catalogue.jsp", "patient.catalogue" },
				{ "/jsp/patient/request.jsp", "patient.request" }, { "/jsp/patient/success.jsp", "patient.success" },
				{ "/jsp/admin/home.jsp", "admin.home" }, { "/jsp/admin/catalogue.jsp", "admin.catalogue" },
				{ "/jsp/admin/success.jsp", "admin.success" }, { "/jsp/doctor/home.jsp", "doctor.home" },
				{ "/jsp/doctor/prescriptions.jsp", "doctor.prescriptions" },
				{ "/jsp/doctor/request.jsp", "doctor.request" },
				{ "/jsp/doctor/requestinfo.jsp", "doctor.requestinfo" },
				{ "/jsp/doctor/success.jsp", "doctor.success" }, { "/jsp/doctor/catalogue.jsp", "doctor.catalogue" },
				{ "/jsp/doctor/addprescription.jsp", "doctor.addprescription" },
				{ "/jsp/admin/addmedicine.jsp", "admin.addmedicine" } });

		return pathAndKey;
	}

	@Test
	public void testMessage() {
		assertEquals(path, PathsManager.getProperty(key));
	}

}
