package by.epam.leki.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import by.epam.leki.database.dao.implementations.UserDAOImpl;
import by.epam.leki.database.dao.interfaces.UserDAO;
import by.epam.leki.service.AuthentificationService;

public class CheckPasswordTest {

	UserDAO userDAO;
	AuthentificationService autServ;

	@Before
	public void initCheck() {
		userDAO = new UserDAOImpl();
		autServ= new AuthentificationService();	}

	@Test
	public void test1() {
		assertTrue(autServ.checkPassword("admin_admin@gmail.com", "123456"));
	}

	@Test
	public void test2() {
		assertFalse(autServ.checkPassword("", ""));
	}

	
}

	