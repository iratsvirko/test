package by.epam.leki.navigation;

import javax.servlet.http.HttpServletRequest;

abstract class ServiceMethod {
	
	public abstract String call(HttpServletRequest request);
}
