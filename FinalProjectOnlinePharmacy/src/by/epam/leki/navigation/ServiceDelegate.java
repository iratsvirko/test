package by.epam.leki.navigation;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import by.epam.leki.service.*;

public class ServiceDelegate {
	private static Map<String, ServiceMethod> mappings;
	
	private static AuthentificationService authServ = new AuthentificationService();
	private static LanguageService langServ = new LanguageService();
	private static NavigationService navServ = new NavigationService();
	private static RegisterService regServ = new RegisterService();
	private static CatalogueService catServ = new CatalogueService();
	private static OrderService ordServ = new OrderService();
	private static RequestService requestServ = new RequestService();
	private static MedicineService medServ = new MedicineService();
	private static PrescriptionService prescServ = new PrescriptionService();
	
	private static final ServiceDelegate INSTANCE = new ServiceDelegate();
	
	/**
	 * Returns instance of ServiceDelegate
	 */
	public static ServiceDelegate getInstance()
	{
		return INSTANCE;
	}
	
	/**
	 * Contains Map with request parameters and matching Service Methods
	 */
	private ServiceDelegate() {
		
		mappings = new HashMap<>();

		mappings.put("prescription_for_med", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return prescServ.preparePresc(request);
			}
		});

		mappings.put("delete_presc", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return prescServ.deletePresc(request);
			}
		});

		mappings.put("add_presc", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return prescServ.addPresc(request);
			}
		});

		mappings.put("language", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return langServ.chooseLang(request);
			}
		});

		mappings.put("extend_presc", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return prescServ.updatePresc(request);
			}
		});
		mappings.put("doc_extend_presc", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return prescServ.preparePresc(request);
			}
		});

		mappings.put("show_presc", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return requestServ.prepareDocRequest(request);
			}
		});

		mappings.put("pat_request", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return requestServ.prepareRequest(request);
			}
		});

		mappings.put("delete_request", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return requestServ.deleteRequest(request);
			}
		});

		mappings.put("make_request", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return requestServ.makeRequest(request);
			}
		});

		mappings.put("login", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return authServ.login(request);
			}
		});
		mappings.put("logout", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return authServ.logout(request);
			}
		});

		mappings.put("regpatient", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return regServ.registerPatient(request);
			}
		});

		mappings.put("guest_med_cat", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return catServ.getGuestMedCatalogue(request);
			}
		});

		mappings.put("doc_med_cat", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return catServ.getDocMedCatalogue(request);
			}
		});

		mappings.put("delete_med", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return medServ.deleteMed(request);
			}
		});
		mappings.put("add_med", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return medServ.addMed(request);
			}
		});

		mappings.put("patient_med_cat", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return catServ.getPatientMedCatalogue(request);
			}
		});
		mappings.put("admin_med_cat", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return catServ.getAdminMedCatalogue(request);
			}
		});

		mappings.put("doc_presc_cat", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return catServ.getDocPrescCatalogue(request);
			}
		});

		mappings.put("patient_presc_cat", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return catServ.getPatientPrescCatalogue(request);
			}
		});
		mappings.put("doc_request_cat", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return catServ.getDocRequestCatalogue(request);
			}
		});

		mappings.put("add_med_page", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return navServ.adminAddMedPage(request);
			}
		});

		mappings.put("login_page", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return navServ.guestLogin(request);
			}
		});

		mappings.put("catalogue", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return navServ.guestCatalogue(request);
			}
		});
		mappings.put("register", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return navServ.guestRegister(request);
			}
		});

		mappings.put("common_home", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return navServ.guestHome(request);
			}
		});

		mappings.put("patient_home", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return navServ.patientHome(request);
			}
		});
		mappings.put("doctor_home", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return navServ.doctorHome(request);
			}
		});
		mappings.put("admin_home", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return navServ.adminHome(request);
			}
		});
		mappings.put("order_patient", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return ordServ.prepareOrder(request);
			}
		});
		mappings.put("order_med", new ServiceMethod() {
			@Override
			public String call(HttpServletRequest request) {
				return ordServ.makeOrder(request);
			}
		});

	}

	public String execute(String param, HttpServletRequest request) {
		return mappings.get(param).call(request);
	}
}
