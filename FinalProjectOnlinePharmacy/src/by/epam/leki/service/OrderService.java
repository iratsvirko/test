package by.epam.leki.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.leki.database.dao.implementations.*;
import by.epam.leki.database.dao.interfaces.*;
import by.epam.leki.entity.*;
import resources.MessageManager;
import resources.PathsManager;

public class OrderService {

	private static final Logger Log = Logger.getLogger(OrderService.class);
	
	private static final String ATTR_LANGUAGE = "language";
	private static final String PARAM_MED = "med_id";
	private static final String PARAM_QUANTITY = "quantity";
	private static final String PAGE_PATIENT_ORDER = "patient.order";
	private static final String PAGE_PATIENT_CATALOGUE = "patient.catalogue";
	private static final String ATTR_MED = "medicine";
	private static final String ATTR_PATIENT = "patient";
	private static final String ATTR_ORDERSUCCEED = "ordersucceed";
	private static final String PAGE_ORDER = "patient.order";
	private static final String PAGE_PATIENT_SUCCEED = "patient.success";
	private static final String ATTR_CHECK_PRESC = "checkpresc";
	private static final String ATTR_UNAVAILABLE = "unavailable";

	private static OrderDAO orderDAO = new OrderDAOImpl();
	private static MedicineDAO medDAO = new MedicineDAOImpl();
	private static PrescriptionDAO prescDAO = new PrescriptionDAOImpl();

	/**
	 *Sets into session attributes which are necessary to make Order and sends user to Order page.
	 * @param request
	 * @return path
	 */
	public String prepareOrder(HttpServletRequest request) {

		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);
		int id = Integer.parseInt(request.getParameter(PARAM_MED));

		if (id == 0) {
			request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
			return PathsManager.getProperty(PAGE_PATIENT_CATALOGUE);

		} else {
			Medicine med = medDAO.getMed(id);
			request.getSession().setAttribute(ATTR_MED, med);
			return PathsManager.getProperty(PAGE_PATIENT_ORDER);
		}

	}

	/**
	 * Adds Patient's order into DB
	 * @param request
	 * @return path
	 */
	public String makeOrder(HttpServletRequest request) {
		
		Patient patient = (Patient) request.getSession().getAttribute(ATTR_PATIENT);
		Medicine med = (Medicine) request.getSession().getAttribute(ATTR_MED);
		int quantity = Integer.parseInt(request.getParameter(PARAM_QUANTITY));
		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);
		Date date = new Date();

		if (med.getId() == 0) {
			request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
			return PathsManager.getProperty(PAGE_ORDER);
		} else {
			Prescription presc = prescDAO.getPrescByPatientMed(patient.getId(), med.getId());

			if (med.getPrescReq() == false || presc.getId() != 0 && (validatePresc(presc, quantity, date)) == true) {

				Order order = new Order();

				order.setMed(med.getName());
				order.setPatientFirstName(patient.getFirstName());
				order.setPatientLastName(patient.getLastName());
				order.setQuantity(quantity);

				if (orderDAO.addOrder(order) == true) {
					request.getSession().removeAttribute(ATTR_MED);

					if (presc.getId() != 0) {
						prescDAO.deletePresc(presc.getId());
					}
					request.setAttribute(ATTR_ORDERSUCCEED, MessageManager.getProperty("message.order_succeed", lang));
					return PathsManager.getProperty(PAGE_PATIENT_SUCCEED);

				} else {
					request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
					return PathsManager.getProperty(PAGE_ORDER);
				}
			} else {
				request.setAttribute(ATTR_CHECK_PRESC, MessageManager.getProperty("message.check_prescription", lang));
				return PathsManager.getProperty(PAGE_ORDER);
			}
		}

	}

	/**
	 * Checks if prescription has still valid date and quantity of medicine, which is requested by user.
	 * @param presc
	 * @param quantity
	 * @param date
	 * @return boolean
	 */
	private boolean validatePresc(Prescription presc, int quantity, Date date) {
		if (convertStringDate(presc.getDate()).getTime() >= date.getTime() && presc.getQuantity() >= quantity)
			return true;
		else {
			return false;
		}

	}

	/**
	 * Converts string date into java.sql.Date format.
	 * @param stringDate
	 * @return java.sql.Date
	 */
	private java.sql.Date convertStringDate(String stringDate) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(stringDate);
		} catch (ParseException e) {
			Log.debug("wrong format of string date", e);
		}
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		return sqlDate;
	}
}