package by.epam.leki.service;

import javax.servlet.http.HttpServletRequest;

import by.epam.leki.entity.*;

import resources.PathsManager;

public class LanguageService {
	
	private static final String USER = "user";
	private static final String LANGUAGE = "language";

	/**
	 * Sets language to the session
	 * @param request
	 * @return path
	 */
	public String chooseLang (HttpServletRequest request){
		
			String strLanguage = request.getParameter(LANGUAGE);
			User user = (User) request.getSession().getAttribute(USER);
			request.getSession().setAttribute(LANGUAGE, strLanguage);

			if (user.getRole() == Role.GUEST) {
				return PathsManager.getProperty("common.home");
			}
			if (user.getRole() == Role.PATIENT) {
				return PathsManager.getProperty("patient.home");
			}
			if (user.getRole() == Role.DOCTOR) {
				return PathsManager.getProperty("doctor.home");
			}
			if (user.getRole() == Role.PHARMASIST) {
				return PathsManager.getProperty("admin.home");
			}
			return PathsManager.getProperty("common.home");
	}}

