package by.epam.leki.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.leki.database.dao.implementations.*;
import by.epam.leki.database.dao.interfaces.*;
import by.epam.leki.entity.*;
import resources.MessageManager;
import resources.PathsManager;

public class RequestService {
	
	private static final Logger Log = Logger.getLogger(DoctorDAO.class);
	
	private static final String ATTR_REQDELETED = "reqdeleted";
	private static final String ATTR_REQADDED = "reqadded";
	private static final String ATTR_LANGUAGE = "language";
	private static final String PARAM_REQUEST = "request_id";
	private static final String PARAM_PRESC = "presc_id";
	private static final String PARAM_PRESC_REQUEST = "request_presc";
	private static final String PARAM_COMMENT = "comment";
	private static final String PAGE_PATIENT_REQUEST = "patient.request";
	private static final String PAGE_DOCTOR_REQUEST_INFO = "doctor.requestinfo";
	private static final String PAGE_DOCTOR_SUCCEED = "doctor.success";
	private static final String PAGE_PATIENT_SUCCEED = "patient.success";
	private static final String PAGE_PATIENT_PRESCRIPTIONS = "patient.prescriptions";
	private static final String PAGE_DOCTOR_REQUEST = "doctor.request";
	private static final String ATTR_PRESC = "prescription";
	private static final String ATTR_REQUEST = "request";
	private static final String ATTR_UNAVAILABLE = "unavailable";
	private static final String ATTR_STILLVALID = "stillvalid";
	private static final String ATTR_REQEXISTS = "reqexists";

	private static RequestDAO reqDAO = new RequestDAOImpl();
	private static PrescriptionDAO prescDAO = new PrescriptionDAOImpl();
	
	/**
	 * Sets into session attributes which are necessary to add new Request
	 * and sends user to Request page.
	 * @param request
	 * @return path
	 */
	public String prepareRequest(HttpServletRequest request) {

		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);
		int id = Integer.parseInt(request.getParameter(PARAM_PRESC));

		Prescription presc = prescDAO.getPresc(id);

		if (presc.getId() == 0) {
			request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
			return PathsManager.getProperty(PAGE_PATIENT_PRESCRIPTIONS);

		} else {
			request.getSession().setAttribute(ATTR_PRESC, presc);
			return PathsManager.getProperty(PAGE_PATIENT_REQUEST);
		}
	}

	/**
	 * Adds new Patient's Request into DB
	 * @param request
	 * @return path
	 */
	public String makeRequest(HttpServletRequest request) {

		Prescription presc = (Prescription) request.getSession().getAttribute(ATTR_PRESC);
		String comment = (String) (request.getParameter(PARAM_COMMENT));
		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);

		if (presc.getId() != 0) {
			if (checkDate(presc.getDate()) == true) {
				if (checkRequest(presc) == false) {
					Request req = new Request();
					
					req.setPrescNumber(presc.getId());
					req.setComment(comment);

					if (reqDAO.addRequest(req) == true) {
						
						request.setAttribute(ATTR_REQADDED, MessageManager.getProperty("message.reqadded", lang));
						return PathsManager.getProperty(PAGE_PATIENT_SUCCEED);
					} else {
						request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
						return PathsManager.getProperty(PAGE_PATIENT_REQUEST);
					}
				} else {
					request.setAttribute(ATTR_REQEXISTS, MessageManager.getProperty("message.reqexists", lang));
					return PathsManager.getProperty(PAGE_PATIENT_REQUEST);
				}
			} else {
				request.setAttribute(ATTR_STILLVALID, MessageManager.getProperty("message.stillvalid", lang));
				return PathsManager.getProperty(PAGE_PATIENT_REQUEST);
			}
		} else {
			request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
			return PathsManager.getProperty(PAGE_PATIENT_PRESCRIPTIONS);
		}

	}

	/**
	 * Sets into session attributes which are necessary to view Request information
	 * and sends Doctor to Doctor's Request information page.
	 * 
	 * @param request
	 * @return path
	 */
	public String prepareDocRequest(HttpServletRequest request) {
		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);
		int prescID = Integer.parseInt(request.getParameter(PARAM_PRESC_REQUEST));
		int reqID = Integer.parseInt(request.getParameter(PARAM_REQUEST));

		Prescription presc = prescDAO.getPresc(prescID);
		Request req = reqDAO.getRequest(reqID);

		if (presc.getId() == 0 || req.getId() == 0) {
			request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
			return PathsManager.getProperty(PAGE_DOCTOR_REQUEST);
		} else {
			request.getSession().setAttribute(ATTR_PRESC, presc);
			request.getSession().setAttribute(ATTR_REQUEST, req);
			return PathsManager.getProperty(PAGE_DOCTOR_REQUEST_INFO);
		}

	}

	/**
	 * Deletes Patient's Request from DB
	 * @param request
	 * @return path
	 */
	public String deleteRequest(HttpServletRequest request) {
		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);
		Request req = (Request) request.getSession().getAttribute(ATTR_REQUEST);

		if (req.getId() != 0) {
			Prescription presc = prescDAO.getPrescByReq(req.getPrescNumber());
			if(presc.getId()!=0){
				prescDAO.deletePresc(presc.getId());
			}
			reqDAO.deleteRequest(req.getId());
			
			request.setAttribute(ATTR_REQDELETED, MessageManager.getProperty("message.reqdeleted", lang));
			return PathsManager.getProperty(PAGE_DOCTOR_SUCCEED);
		} else {
			request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
			return PathsManager.getProperty(PAGE_DOCTOR_REQUEST_INFO);

		}
	}

	/**
	 * Checks if exists Request for exact Prescription in DB
	 * @param presc
	 * @return path
	 */
	private boolean checkRequest(Prescription presc) {
		Request req = reqDAO.getRequestByPresc(presc.getId());
		if (req.getId() != 0)
			return true;
		else {
			return false;
		}
	}

	/**
	 * Checks if Prescription Valid date is expired
	 * @param stringDate
	 * @return path
	 */
	
	private boolean checkDate(String stringDate) {
		
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(stringDate);
		} catch (ParseException e) {
			Log.debug("Exception parsing date", e);
		}
		Date currentDate = new Date();

		if (date.getTime() < currentDate.getTime())
			return true;
		else {
			return false;
		}

	}
}
