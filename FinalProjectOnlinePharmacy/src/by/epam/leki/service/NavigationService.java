package by.epam.leki.service;

import javax.servlet.http.HttpServletRequest;

import resources.PathsManager;

public class NavigationService {
	private static final String PAGE_GUEST_HOME = "common.home";
	private static final String PAGE_GUEST_CATALOGUE = "common.catalogue";
	private static final String PAGE_GUEST_REGISTER = "common.register";
	private static final String PAGE_GUEST_LOGIN = "common.login";
	private static final String PAGE_PATIENT_HOME = "patient.home";
	private static final String PAGE_DOCTOR_HOME = "doctor.home";
	private static final String PAGE_ADMIN_HOME = "admin.home";
	private static final String PAGE_ADMIN_ADD_MED = "admin.addmedicine";
	
	/**
	 * Returns path to Guest home page
	 * @param request
	 * @return path
	 */
	public String guestHome(HttpServletRequest request) {
		return PathsManager.getProperty(PAGE_GUEST_HOME);
	}

	/**
	 * Returns path to Guest catalog of medicines
	 * @param request
	 * @return path
	 */
	public String guestCatalogue(HttpServletRequest request) {
		return PathsManager.getProperty(PAGE_GUEST_CATALOGUE);
	}

	/**
	 * Returns path to Guest register page
	 * @param request
	 * @return path
	 */
	public String guestRegister(HttpServletRequest request) {
		return PathsManager.getProperty(PAGE_GUEST_REGISTER);
	}

	/**
	 * Returns path to Guest login page
	 * @param request
	 * @return path
	 */
	public String guestLogin(HttpServletRequest request) {
		return PathsManager.getProperty(PAGE_GUEST_LOGIN);
	}

	/**
	 * Returns path to Patient home page
	 * @param request
	 * @return path
	 */
	public String patientHome(HttpServletRequest request) {
		return PathsManager.getProperty(PAGE_PATIENT_HOME);
	}
	
	/**
	 * Returns path to Doctor home page
	 * @param request
	 * @return path
	 */
	public String doctorHome(HttpServletRequest request) {
		return PathsManager.getProperty(PAGE_DOCTOR_HOME);
	}

	/**
	 * Returns path to Admin home page
	 * @param request
	 * @return path
	 */
	public String adminHome(HttpServletRequest request) {
		return PathsManager.getProperty(PAGE_ADMIN_HOME);
	}
	
	/**
	 * Returns path to page, where Admin can add new Medicine
	 * @param request
	 * @return path
	 */
	public String adminAddMedPage(HttpServletRequest request) {
		return PathsManager.getProperty(PAGE_ADMIN_ADD_MED);
	}
	
	
	
}
