package by.epam.leki.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.leki.database.dao.implementations.*;
import by.epam.leki.database.dao.interfaces.*;
import by.epam.leki.entity.*;
import resources.PathsManager;

public class CatalogueService {

	private static final String PAGE_GUEST_CATALOGUE_MEDS = "common.catalogue";
	private static final String PAGE_DOCTOR_CATALOGUE_REQUESTS = "doctor.request";
	private static final String PAGE_PATIENT_CATALOGUE_MEDS = "patient.catalogue";
	private static final String PAGE_ADMIN_CATALOGUE_MEDS = "admin.catalogue";
	private static final String PAGE_DOC_CATALOGUE_PRESCS = "doctor.prescriptions";
	private static final String PAGE_DOC_CATALOGUE_MEDS = "doctor.catalogue";
	private static final String PAGE_PATIENT_CATALOGUE_PRESCS = "patient.prescriptions";
	private static final String ATTR_MEDICINES = "medicines";
	private static final String ATTR_PRECRIPTIONS = "prescriptions";
	private static final String ATTR_REQUESTS = "requests";
	private static final String ATTR_PATIENT = "patient";
	private static final String ATTR_DOC = "doctor";
	private static final String ATTR_PAGES = "pages";
	private static final String PARAM_NUM = "num";

	private static RequestDAO reqDAO = new RequestDAOImpl();
	private static MedicineDAO medDAO = new MedicineDAOImpl();
	private static PrescriptionDAO prescDAO = new PrescriptionDAOImpl();

	/**
	 * Gets Medicines catalog in form, which is available for Guest
	 * 
	 * @param request
	 * @return path
	 */
	public String getGuestMedCatalogue(HttpServletRequest request) {
		int numberOfPages = medDAO.getNumberOfPages();
		request.setAttribute(ATTR_PAGES, numberOfPages);
		String num = request.getParameter(PARAM_NUM);
		int offset = Integer.parseInt(num) * 10 - 10;
		List<Medicine> list = medDAO.getMedsCatal(offset);
		request.setAttribute(ATTR_MEDICINES, list);
		return PathsManager.getProperty(PAGE_GUEST_CATALOGUE_MEDS);
	}

	/**
	 * Gets Medicines catalog in form, which is available for Patient
	 * 
	 * @param request
	 * @return path
	 */
	
	public String getPatientMedCatalogue(HttpServletRequest request) {
		int numberOfPages = medDAO.getNumberOfPages();
		request.setAttribute(ATTR_PAGES, numberOfPages);
		int offset = Integer.parseInt(request.getParameter(PARAM_NUM)) * 10 - 10;
		List<Medicine> list = medDAO.getMedsCatal(offset);
		request.setAttribute(ATTR_MEDICINES, list);
		return PathsManager.getProperty(PAGE_PATIENT_CATALOGUE_MEDS);
	}

	/**
	 * Gets Medicines catalog in form, which is available for Doctor
	 * 
	 * @param request
	 * @return path
	 */
	
	public String getDocMedCatalogue(HttpServletRequest request) {
		List<Medicine> list = medDAO.getMedsCatalPresc();
		request.setAttribute(ATTR_MEDICINES, list);
		return PathsManager.getProperty(PAGE_DOC_CATALOGUE_MEDS);
	}

	/**
	 * Gets Medicines catalog in form, which is available for Admin
	 * 
	 * @param request
	 * @return path
	 */

	public String getAdminMedCatalogue(HttpServletRequest request) {

		int numberOfPages = medDAO.getNumberOfPages();
		request.setAttribute(ATTR_PAGES, numberOfPages);
		int offset = Integer.parseInt(request.getParameter(PARAM_NUM)) * 10 - 10;
		List<Medicine> list = medDAO.getMedsCatal(offset);
		request.setAttribute(ATTR_MEDICINES, list);
		return PathsManager.getProperty(PAGE_ADMIN_CATALOGUE_MEDS);
	}

	/**
	 * Gets catalog of Prescriptions, which were signed by certain Doctor
	 * 
	 * @param request
	 * @return path
	 */
	public String getDocPrescCatalogue(HttpServletRequest request) {
		Doctor doc = (Doctor) request.getSession().getAttribute(ATTR_DOC);
		List<Prescription> list = prescDAO.getPrescsCatalDoc(doc.getId());
		request.setAttribute(ATTR_PRECRIPTIONS, list);
		return PathsManager.getProperty(PAGE_DOC_CATALOGUE_PRESCS);

	}

	/**
	 * Gets catalog of Prescriptions, which were signed for certain Patient
	 * 
	 * @param request
	 * @return path
	 */
	public String getPatientPrescCatalogue(HttpServletRequest request) {
		Patient patient = (Patient) request.getSession().getAttribute(ATTR_PATIENT);
		List<Prescription> list = prescDAO.getPrescsCatalByID(patient.getId());
		request.setAttribute(ATTR_PRECRIPTIONS, list);
		return PathsManager.getProperty(PAGE_PATIENT_CATALOGUE_PRESCS);

	}

	/**
	 * Gets catalog of Requests on extension of Prescriptions, which were added
	 * by certain Doctor.
	 * 
	 * @param request
	 * @return path
	 */
	public String getDocRequestCatalogue(HttpServletRequest request) {
		Doctor doc = (Doctor) request.getSession().getAttribute(ATTR_DOC);
		List<Request> list = reqDAO.getRequestCatalDoc(doc.getId());
		request.setAttribute(ATTR_REQUESTS, list);
		return PathsManager.getProperty(PAGE_DOCTOR_CATALOGUE_REQUESTS);

	}

}
