package by.epam.leki.service;

import javax.servlet.http.HttpServletRequest;

import by.epam.leki.database.dao.implementations.*;
import by.epam.leki.database.dao.interfaces.*;
import by.epam.leki.entity.*;
import resources.MessageManager;
import resources.PathsManager;

public class AuthentificationService {
	
	private static final String LANGUAGE = "language";
	private static final String ATTR_CHECKLOGIN = "checklogin";
	private static final String ATTR_USER = "user";
	private static final String ATTR_PATIENT = "patient";
	private static final String ATTR_DOCTOR = "doctor";
	private static final String PARAM_EMAIL = "email";
	private static final String PARAM_PASSWORD = "password";
	private static final String PAGE_GUEST_HOME = "common.home";
	private static final String PAGE_LOGIN = "common.login";
	private static final String PAGE_PATIENT_HOME = "patient.home";
	private static final String PAGE_DOCTOR_HOME = "doctor.home";
	private static final String PAGE_ADMIN_HOME = "admin.home";

	private static UserDAO userDAO = new UserDAOImpl();
	private static PatientDAO patDAO = new PatientDAOImpl();
	private static DoctorDAO docDAO = new DoctorDAOImpl();

	/**
	 * Log in User in application
	 * @param request
	 * @return path
	 */
	public String login(HttpServletRequest request) {

		{
			String lang = (String) request.getSession().getAttribute(LANGUAGE);
			String email = request.getParameter(PARAM_EMAIL);
			String password = request.getParameter(PARAM_PASSWORD);
			User user = null;

			if (checkPassword(email, password)) {
				user = userDAO.getUser(email);
				request.getSession().setAttribute(ATTR_USER, user);
			} else {
				request.setAttribute(ATTR_CHECKLOGIN, MessageManager.getProperty("message.check_login", lang));
				return PathsManager.getProperty(PAGE_LOGIN);
			}
			if (user.getRole() == Role.PATIENT) {
				Patient patient = patDAO.getPatient(email);
				request.getSession().setAttribute(ATTR_PATIENT, patient);
				return PathsManager.getProperty(PAGE_PATIENT_HOME);
			} else if (user.getRole() == Role.DOCTOR) {
				Doctor doctor = docDAO.getDoctor(userDAO.getUser(email).getId());
				request.getSession().setAttribute(ATTR_DOCTOR, doctor);
				return PathsManager.getProperty(PAGE_DOCTOR_HOME);
			} else if (user.getRole() == Role.PHARMASIST) {
				return PathsManager.getProperty(PAGE_ADMIN_HOME);
			} else {
				return PathsManager.getProperty(PAGE_GUEST_HOME);
			}
		}
	}

	/**
	 * Log out User from application
	 * @param request
	 * @return path
	 */
	public String logout(HttpServletRequest request) {

		if (request.getSession().getAttribute(ATTR_USER) != null) {
			request.getSession().removeAttribute(ATTR_USER);
		}
		return PathsManager.getProperty(PAGE_GUEST_HOME);
	}


	/**
	 * Checks, if exists User with such email and password in DB
	 * @param email
	 * @param password
	 * @return boolean
	 */
	public boolean checkPassword(String email, String password) {

		String dbpass = userDAO.getUser(email).getPassword();
		if (password.equals(dbpass)) {
			return true;
		} else {
			return false;
		}
	}

	
}
