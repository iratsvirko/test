package by.epam.leki.service;

import javax.servlet.http.HttpServletRequest;

import by.epam.leki.database.dao.implementations.*;
import by.epam.leki.database.dao.interfaces.*;
import by.epam.leki.entity.*;
import resources.MessageManager;
import resources.PathsManager;

public class MedicineService {

	private static final String PARAM_MED_NAME = "name";
	private static final String PARAM_ACT_SUBST = "actSubst";
	private static final String PARAM_MED_FORM = "medForm";
	private static final String PARAM_PRICE = "price";
	private static final String PARAM_PRESC = "prescReq";
	private static final String ATTR_UNAVAILABLE = "unavailable";
	private static final String ATTR_MEDDELETED = "meddeleted";
	private static final String ATTR_MEDADDED = "meddadded";
	private static final String ATTR_LANGUAGE = "language";
	private static final String PARAM_MED = "med_id";
	private static final String PAGE_ADMIN_SUCCEED = "admin.success";
	private static final String PAGE_ADMIN_CATALOGUE = "admin.catalogue";
	private static final String PAGE_ADMIN_ADDMEDICINE = "admin.addmedicine";
	private static final String ATTR_MEDEXISTS = "medexists";
	private static final String ATTR_DONT_EXIST = "dontexist";
	
	private static MedicineDAO medService = new MedicineDAOImpl();

	/**
	 * Deletes Medicine
	 * @param request
	 * @return path
	 */
	public String deleteMed(HttpServletRequest request) {
		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);
		int id = Integer.parseInt(request.getParameter(PARAM_MED));

		if (medService.deleteMed(id) == true) {

			request.setAttribute(ATTR_MEDDELETED, MessageManager.getProperty("message.meddeleted", lang));
			return PathsManager.getProperty(PAGE_ADMIN_SUCCEED);
		} else {
			request.setAttribute(ATTR_DONT_EXIST, MessageManager.getProperty("message.dontexist", lang));
			return PathsManager.getProperty(PAGE_ADMIN_CATALOGUE);
		}

	}

	/**
	 * Adds new Medicine
	 * @param request
	 * @return path
	 */
	public String addMed(HttpServletRequest request) {

		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);
		String name = (String) request.getParameter(PARAM_MED_NAME);
		String actSubst = (String) request.getParameter(PARAM_ACT_SUBST);
		String medForm = (String) request.getParameter(PARAM_MED_FORM);
		float price = Float.parseFloat(request.getParameter(PARAM_PRICE));
		boolean pescReq = Boolean.parseBoolean(request.getParameter(PARAM_PRESC));

		if (checkMedicine(name)) {
			
			Medicine med = new Medicine();

			med.setName(name);
			med.setActSubst(actSubst);
			med.setMedForm(medForm);
			med.setPrice(price);
			med.setPrescReq(pescReq);

			if (medService.addMed(med) == true) {

				request.setAttribute(ATTR_MEDADDED, MessageManager.getProperty("message.medadded", lang));
				return PathsManager.getProperty(PAGE_ADMIN_SUCCEED);
			}

			else {
				request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
				return PathsManager.getProperty(PAGE_ADMIN_ADDMEDICINE);
			}
		} else {
			request.setAttribute(ATTR_MEDEXISTS, MessageManager.getProperty("message.med_exists", lang));
			return PathsManager.getProperty(PAGE_ADMIN_ADDMEDICINE);

		}
	}

	/**
	 * Checks if input medicine exists in DB
	 * @param medName
	 * @return boolean
	 */
	private boolean checkMedicine(String medName) {
		String dbMedName = medService.getMedByName(medName).getName();
		if (!medName.equals(dbMedName)) {
			return true;
		} else 
		{
			return false;
		}
	}

}
