package by.epam.leki.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.leki.database.dao.implementations.*;
import by.epam.leki.database.dao.interfaces.*;
import by.epam.leki.entity.*;
import resources.MessageManager;
import resources.PathsManager;

public class PrescriptionService {

	private static final Logger Log = Logger.getLogger(PrescriptionService.class);

	private static final String PARAM_NEWDATE = "new_date";
	private static final String ATTR_REQEXTENDED = "reqextended";
	private static final String PARAM_FIRST_NAME = "patientFirstName";
	private static final String PARAM_LAST_NAME = "patientLastName";
	private static final String PARAM_QUANTITY = "quantity";
	private static final String PARAM_DATE = "date";
	private static final String PAGE_DOCTOR_CATALOGUE = "doctor.catalogue";
	private static final String PAGE_DOCTOR_ADDPRESCRIPTION = "doctor.addprescription";
	private static final String PAGE_DOCTOR_PRESCRIPTIONS = "doctor.prescriptions";
	private static final String ATTR_DOCTOR = "doctor";
	private static final String PAGE_DOCTOR_SUCCEED = "doctor.success";
	private static final String ATTR_PRESCDELETED = "prescdeleted";
	private static final String ATTR_PRESCADDED = "prescadded";
	private static final String ATTR_LANGUAGE = "language";
	private static final String PARAM_MED = "med_id";
	private static final String PARAM_PRESC = "presc_id";
	private static final String ATTR_MED = "medicine";
	private static final String ATTR_UNAVAILABLE = "unavailable";
	private static final String ATTR_PRESCEXISTS = "prescexists";
	private static final String ATTR_NOPATIENT = "nopatient";
	private static final String ATTR_PRESC = "prescription";
	private static final String ATTR_REQUEST = "request";
	private static final String PAGE_DOCTOR_REQUEST_INFO = "doctor.requestinfo";
	private static final String ATTR_CHECKDATE = "checkdate";
	private static final String ATTR_PRESCHASREQ = "preschasreq";

	private static PatientDAO patDAO = new PatientDAOImpl();
	private static RequestDAO reqDAO = new RequestDAOImpl();
	private static PrescriptionDAO prescDAO = new PrescriptionDAOImpl();
	private static MedicineDAO medDAO = new MedicineDAOImpl();

	/**
	 * Sets into session attributes which are necessary to add new Prescription
	 * and sends user to Order page.
	 * 
	 * @param request
	 * @return path
	 */

	public String preparePresc(HttpServletRequest request) {

		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);
		int id = Integer.parseInt(request.getParameter(PARAM_MED));
		Medicine med = medDAO.getMed(id);

		if (med.getId() == 0) {
			request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
			return PathsManager.getProperty(PAGE_DOCTOR_CATALOGUE);

		} else {
			request.getSession().setAttribute(ATTR_MED, med);
			return PathsManager.getProperty(PAGE_DOCTOR_ADDPRESCRIPTION);
		}

	}

	/**
	 * Adds new Doctor's Prescription into DB
	 * 
	 * @param request
	 * @return path
	 */
	public String addPresc(HttpServletRequest request) {
		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);
		Doctor doc = (Doctor) request.getSession().getAttribute(ATTR_DOCTOR);
		Medicine med = (Medicine) request.getSession().getAttribute(ATTR_MED);
		String patientFirstName = request.getParameter(PARAM_FIRST_NAME);
		String patientLastName = request.getParameter(PARAM_LAST_NAME);
		int quantity = Integer.parseInt(request.getParameter(PARAM_QUANTITY));
		Patient patient = patDAO.getPatientByName(patientFirstName, patientLastName);

		String date = request.getParameter(PARAM_DATE);
		if (checkDate(date) == true) {
			if (med.getId() == 0) {
				request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
				return PathsManager.getProperty(PAGE_DOCTOR_ADDPRESCRIPTION);

			} else {
				if (patient.getId() == 0) {
					request.setAttribute(ATTR_NOPATIENT, MessageManager.getProperty("message.nopatient", lang));
					return PathsManager.getProperty(PAGE_DOCTOR_ADDPRESCRIPTION);
				} else {
					if (checkPresc(patient, med) == true) {
						Prescription presc = new Prescription();

						presc.setMedName(med.getName());
						presc.setPatientFirstName(patientFirstName);
						presc.setPatientLastName(patientLastName);
						presc.setDoctorName(doc.getName());
						presc.setQuantity(quantity);
						presc.setDate(date);

						if (prescDAO.addPresc(presc) == true) {
							request.setAttribute(ATTR_PRESCADDED,
									MessageManager.getProperty("message.prescadded", lang));
							return PathsManager.getProperty(PAGE_DOCTOR_SUCCEED);
						} else {
							request.setAttribute(ATTR_UNAVAILABLE,
									MessageManager.getProperty("message.unavailable", lang));
							return PathsManager.getProperty(PAGE_DOCTOR_ADDPRESCRIPTION);
						}
					} else {
						request.setAttribute(ATTR_PRESCEXISTS, MessageManager.getProperty("message.prescexists", lang));
						return PathsManager.getProperty(PAGE_DOCTOR_ADDPRESCRIPTION);

					}
				}
			}
		} else {
			request.setAttribute(ATTR_CHECKDATE, MessageManager.getProperty("message.checkdate", lang));
			return PathsManager.getProperty(PAGE_DOCTOR_ADDPRESCRIPTION);

		}
	}

	/**
	 * Updates Prescription, changing old Valid date on chosen by Doctor
	 * 
	 * @param request
	 * @return path
	 */
	public String updatePresc(HttpServletRequest request) {

		Prescription presc = (Prescription) request.getSession().getAttribute(ATTR_PRESC);
		Request req = (Request) request.getSession().getAttribute(ATTR_REQUEST);
		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);
		String newdate = (String) (request.getParameter(PARAM_NEWDATE));
		Medicine med = medDAO.getMedByName(presc.getMedName());
		if (checkDate(newdate) == true) {
			if (presc.getId() != 0 && req.getId() != 0&& med.getId()!=0) {
				prescDAO.updatePresc(presc.getId(), newdate);
				reqDAO.deleteRequest(req.getId());
				request.setAttribute(ATTR_REQEXTENDED, MessageManager.getProperty("message.reqextended", lang));
				return PathsManager.getProperty(PAGE_DOCTOR_SUCCEED);
			} else {
				request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
				return PathsManager.getProperty(PAGE_DOCTOR_REQUEST_INFO);
			}
		} else {
			request.setAttribute(ATTR_CHECKDATE, MessageManager.getProperty("message.checkdate", lang));
			return PathsManager.getProperty(PAGE_DOCTOR_REQUEST_INFO);
		}
	}

	/**
	 * Deletes Prescription from DB, if there is no requests left on this
	 * prescription.
	 * 
	 * @param request
	 * @return path
	 */
	public String deletePresc(HttpServletRequest request) {
		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);
		int id = Integer.parseInt(request.getParameter(PARAM_PRESC));

		if (reqDAO.getRequestByPresc(id).getId() == 0) {

			if (prescDAO.deletePresc(id) == true) {

				request.setAttribute(ATTR_PRESCDELETED, MessageManager.getProperty("message.prescdeleted", lang));
				return PathsManager.getProperty(PAGE_DOCTOR_SUCCEED);
			} else {
				request.setAttribute(ATTR_UNAVAILABLE, MessageManager.getProperty("message.unavailable", lang));
				return PathsManager.getProperty(PAGE_DOCTOR_PRESCRIPTIONS);
			}
		} else {
			request.setAttribute(ATTR_PRESCHASREQ, MessageManager.getProperty("message.preschasreq", lang));
			return PathsManager.getProperty(PAGE_DOCTOR_PRESCRIPTIONS);

		}
	}

	/**
	 * Checks if exists prescription with exact combination of patient and
	 * medicine in database
	 * 
	 * @param patient
	 * @param med
	 * @return boolean
	 */
	private boolean checkPresc(Patient patient, Medicine med) {

		Prescription presc = prescDAO.getPrescByPatientMed(patient.getId(), med.getId());
		if (presc.getId() == 0)
			return true;
		else {
			return false;
		}

	}

	/**
	 * Checks if validity date of the prescription is later than current date
	 * 
	 * @param stringDate
	 * @return boolean
	 */
	private boolean checkDate(String stringDate) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(stringDate);
		} catch (ParseException e) {
			Log.debug("Exception parsing date", e);
		}
		Date currentDate = new Date();
		if (date.getTime() > currentDate.getTime())

			return true;
		else {
			return false;
		}

	}

}
