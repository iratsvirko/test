package by.epam.leki.service;

import javax.servlet.http.HttpServletRequest;

import by.epam.leki.database.dao.implementations.*;
import by.epam.leki.database.dao.interfaces.*;
import by.epam.leki.entity.*;
import resources.MessageManager;
import resources.PathsManager;

public class RegisterService {
	
	private static final String ATTR_LANGUAGE = "language";
	private static final String ATTR_EMAILEXISTS = "emailexists";
	private static final String ATTR_PATIENT = "patient";
	private static final String PARAM_EMAIL = "email";
	private static final String PARAM_PASSWORD = "password";
	private static final String PARAM_FIRST_NAME = "firstName";
	private static final String PARAM_LAST_NAME = "lastName";
	private static final String PARAM_PHONE_NUMBER = "phoneNumber";
	private static final String PARAM_CARD_NUMBER = "cardNumber";
	private static final String PAGE_PATIENT_HOME = "patient.home";
	private static final String PAGE_REGISTER = "common.register";
	
	private static PatientDAO patientDAO = new PatientDAOImpl();
	private static UserDAO  userDAO = new UserDAOImpl();
	
	/**
	 * Adds new Patient into DB
	 * @param request
	 * @return path
	 */
	public String registerPatient(HttpServletRequest request) {

		String email = request.getParameter(PARAM_EMAIL);
		String password = request.getParameter(PARAM_PASSWORD);
		String firstName = request.getParameter(PARAM_FIRST_NAME);
		String lastName = request.getParameter(PARAM_LAST_NAME);
		String phoneNumber = request.getParameter(PARAM_PHONE_NUMBER);
		String cardNumder = request.getParameter(PARAM_CARD_NUMBER);
		String lang = (String) request.getSession().getAttribute(ATTR_LANGUAGE);

			if (checkEmail(email)) {
				
				Patient patient = new Patient();
				
				patient.setRole(Role.PATIENT);
				patient.setEmail(email);
				patient.setPassword(password);
				patient.setFirstName(firstName);
				patient.setLastName(lastName);
				patient.setPhoneNumber(phoneNumber);
				patient.setCardNumber(cardNumder);
			
		
				patientDAO.addPatient(patient);
				
				Patient currPatient = patientDAO.getPatient(request.getParameter(PARAM_EMAIL));
				request.getSession().setAttribute(ATTR_PATIENT, currPatient);
				return PathsManager.getProperty(PAGE_PATIENT_HOME);
			} else {
				request.setAttribute(ATTR_EMAILEXISTS, MessageManager.getProperty("message.email_exists", lang));
				return PathsManager.getProperty(PAGE_REGISTER);
			}
		}
	
	/**
	 * Checks if exists User with the same email in DB
	 * @param email
	 * @return boolean
	 */
	private boolean checkEmail(String email) {
		
		String dbemail = userDAO.getUser(email).getEmail();
		if (!email.equals(dbemail)) {
			return true;
		} else {
			return false;
		}
	}
}
