package by.epam.leki.customtags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import by.epam.leki.database.ConnectionPool;
import org.apache.log4j.Logger;

public class PrescTabTag extends TagSupport {

	private static final long serialVersionUID = 1078414489947029929L;
	private static final Logger Log = Logger.getLogger(ConnectionPool.class);
	private String medName;
	private String patientFirstName;
	private String patientLastName;
	private String doctorName;
	private int quantity;
	private String date;

	@Override
	public int doStartTag() throws JspException {
		try {
			pageContext.getOut()
					.write("<td>" + medName + "</td><td>" + patientFirstName + "</td><td>" + patientLastName
							+ "</td><td>" + doctorName + "</td><td>" + quantity + "</td><td>" + date
							+ "</td>");
		} catch (IOException e) {
			Log.error("IOException in custom tag");
		}
		return SKIP_BODY;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getMedName() {
		return medName;
	}

	public void setMedName(String medName) {
		this.medName = medName;
	}

	public String getPatientFirstName() {
		return patientFirstName;
	}

	public void setPatientFirstName(String patientFirstName) {
		this.patientFirstName = patientFirstName;
	}

	public String getPatientLastName() {
		return patientLastName;
	}

	public void setPatientLastName(String patientLastName) {
		this.patientLastName = patientLastName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

}
