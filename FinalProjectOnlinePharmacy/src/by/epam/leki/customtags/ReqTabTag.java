package by.epam.leki.customtags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import by.epam.leki.database.ConnectionPool;
import org.apache.log4j.Logger;

public class ReqTabTag extends TagSupport {

	private static final long serialVersionUID = 1078414489947029929L;
	private static final Logger Log = Logger.getLogger(ConnectionPool.class);
	
	private int prescNumber;
	private String comment;

	@Override
	public int doStartTag() throws JspException {
		try {
			pageContext.getOut()
					.write("<td>" + prescNumber + "</td><td>" + comment + "</td>");
		} catch (IOException e) {
			Log.error("IOException in custom tag");
		}
		return SKIP_BODY;
	}

	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getPrescNumber() {
		return prescNumber;
	}

	public void setPrescNumber(int prescNumber) {
		this.prescNumber = prescNumber;
	}

}
