package by.epam.leki.customtags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import by.epam.leki.database.ConnectionPool;
import org.apache.log4j.Logger;

public class MedTabTag extends TagSupport {

	private static final long serialVersionUID = 1078414489947029929L;
	private static final Logger Log = Logger.getLogger(ConnectionPool.class);
	
	private String name;
	private String actSubst;
	private String medForm;
	private float price;
	private boolean prescReq;
	
		@Override
	public int doStartTag() throws JspException {
		try {
			pageContext.getOut()
					.write("<td><strong>" + name + "</strong></td><td>" + actSubst + "</td><td>" + medForm
							+ "</td><td>" + price + "</td><td>" + prescReq + "</td>");
		} catch (IOException e) {
			Log.error("IOException in custom tag");
		}
		return SKIP_BODY;
	}
	
	
	public boolean getPrescReq() {
		return prescReq;
	}
	public void setPrescReq(boolean prescReq) {
		this.prescReq = prescReq;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getActSubst() {
		return actSubst;
	}
	public void setActSubst(String actSubst) {
		this.actSubst = actSubst;
	}
	public String getMedForm() {
		return medForm;
	}
	public void setMedForm(String medForm) {
		this.medForm = medForm;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}

}
