package by.epam.leki.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.epam.leki.entity.Role;
import by.epam.leki.entity.User;

@WebFilter(urlPatterns = { "/controller" }, servletNames = { "Controller" })

public class ServletSecurityFilter implements Filter {
	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		User user  = (User) session.getAttribute("user");

		if ( user == null) {
			User newUser  = new User();
			newUser.setRole(Role.GUEST);
		session.setAttribute("user", newUser);
		RequestDispatcher dispatcher = request.getServletContext()
		.getRequestDispatcher("/jsp/common/home.jsp");
		dispatcher.forward(req, resp);
		return;
		}
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}
}