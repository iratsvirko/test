<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%><html>
<%@ taglib prefix="ctg" uri="customtags"%>
<body>
	<div class="left_content">
		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="label.prescription" />

		</div>

		<div class="med_catalogue">
			<p class="details">${unavailable}</p>
			<table class="cart_table">
				<tr class="cart_title">
					<td></td>
					<td><fmt:message key="info.table_med_name" /></td>
					<td><fmt:message key="info.table_pat_fname" /></td>
					<td><fmt:message key="info.table_pat_lname" /></td>
					<td><fmt:message key="info.table_doc_name" /></td>
					<td><fmt:message key="info.table_quan" /></td>
					<td><fmt:message key="info.table_validity" /></td>

				</tr>
				<c:forEach var="prescription" items="${prescriptions}">
					<tr>
						<td>
							<form action="Controller" method="post">
								<input type="hidden" name="param" value="pat_request" /> <input
									type="hidden" name="presc_id" value="${prescription.id}" /> <a><input
									type="submit" value="<fmt:message key="button.extend" />" /></a>
							</form>
						</td>
						<ctg:presctab medName="${prescription.medName}"
							patientFirstName="${prescription.patientFirstName}"
							patientLastName="${prescription.patientLastName}"
							doctorName="${prescription.doctorName}"
							quantity="${prescription.quantity}" date="${prescription.date}" />

					</tr>
				</c:forEach>
			</table>

		</div>

		<div class="clear"></div>

	</div>

</body>
</html>