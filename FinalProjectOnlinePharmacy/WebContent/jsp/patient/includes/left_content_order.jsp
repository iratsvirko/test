<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%><html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<body>
	<div class="left_content">
		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="label.order" />
		</div>
		<div class="clear"></div>

		<div class="feat_prod_box_details">
			${dontexist}
			<div class="form_row">${checkpresc}${unavailable}</div>

			<div class="contact_form">
				<div class="form_subtitle">
					<fmt:message key="label.makeorder" />
				</div>


				<form action="Controller" method="post">
					<input type="hidden" name="param" value="order_med" />

					<div class="form_row">


						<label class="contact"><strong><fmt:message
									key="label.medicine" /></strong></label>
						<p>${medicine.name}
						<p>
					</div>


					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.firstName" /></strong></label>
						<p>${patient.firstName}
						<p>
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.lastName" /></strong></label>
						<p>${patient.lastName}
						<p>
					</div>


					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.quantity" /></strong></label> <input type="text"
							class="contact_input" name="quantity" value=0 />
					</div>


					<div class="form_row">
						<input type="submit" class="register" onclick="check_form()"
							value=<fmt:message key="button.order" /> />
					</div>


				</form>
			</div>

		</div>


		<div class="clear"></div>
	</div>
</body>
</html>