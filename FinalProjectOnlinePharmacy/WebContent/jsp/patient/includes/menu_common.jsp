<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>

<body>
	<div id="menu">
		<ul>

			<li>
				<form action="Controller" method="post">
					<input type="hidden" name="param" value="patient_home" /> <a><input
						type="submit" value="<fmt:message key="menu.main" />" /></a>
				</form>
			</li>
			<li>
				<form action="Controller" method="post">
					<input type="hidden" name="param" value="patient_med_cat" /> <input
						type="hidden" name="num" value="1" /> <a><input type="submit"
						value="<fmt:message key="menu.catalogue" />" /></a>
				</form>
			</li>
			<li>
				<form action="Controller" method="post">
					<input type="hidden" name="param" value="patient_presc_cat" /> <a><input
						type="submit" value="<fmt:message key="menu.prescription" />" /></a>
				</form>
			</li>

			<li>
				<form action="Controller" method="post">
					<input type="hidden" name="param" value="logout" /> <a><input
						type="submit" value="<fmt:message key="menu.logout" />" /></a>
				</form>
			</li>

			<li><a>${patient.firstName }</a></li>
		</ul>
	</div>





</body>
</html>