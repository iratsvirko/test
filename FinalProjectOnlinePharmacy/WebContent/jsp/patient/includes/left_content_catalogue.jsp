<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/jsp/common/includes/set_language.jsp"%><html>
<%@ taglib prefix="ctg" uri="customtags"%>
<script src="./js/jquery-3.1.1.min.js"></script>
<script src="./js/table_image.js"></script>

<body>
	<div class="left_content">
		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="label.catalogue" />
		</div>

		<div class="med_catalogue">


    <p class="details">
            ${unavailable } 
            </p>
            
			<table class="cart_table">
				<tr class="cart_title">
							<td></td>
					<td><fmt:message key="info.table_med_name" /></td>
					<td><fmt:message key="info.table_act_subst" /></td>
					<td><fmt:message key="info.table_med_form" /></td>
					<td><fmt:message key="info.table_price" /></td>
					<td><fmt:message key="info.table_presc_req" /></td>
			
				</tr>
				<c:forEach var="medicine" items="${medicines}">
					<tr>
						<td>
							<form action="Controller" method="post">
								<input type="hidden" name="param" value="order_patient" /> 
								<input type="hidden" name="med_id" value="${medicine.id}" />
								<a><input type="submit" value="<fmt:message key="button.order" />" /></a>
							</form>
						</td>
						<ctg:medtab name="${medicine.name}"
							actSubst="${medicine.actSubst}" medForm="${medicine.medForm}"
							price="${medicine.price}" prescReq="${medicine.prescReq}" />
						
					</tr>
				</c:forEach>
			</table>
	
		<table class="num_table">
			<tr>
				<c:forEach begin="1" end="${pages}" varStatus="loop">
					<td><form action="Controller" method="post">
							<input type="hidden" name="param" value="patient_med_cat" />
							<input type="hidden" name="num" value="${loop.index}" />
							 <input	type="submit" value="${loop.index}" />
						</form></td>
				</c:forEach>
			</tr>
		</table>



		
		</div>

		<div class="clear"></div>

	</div>






</body>
</html>