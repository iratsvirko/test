<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@include file="/jsp/common/includes/set_language.jsp"%>
<html>

<body>
	<div class="head_content">
		<form action="Controller" method="post">
			<input type="hidden" name="param" value="admin_home" /> <input
				type="image" src="images/logo.gif"
				alt=<fmt:message key="menu.main"/> class="logo">
		</form>

		<jsp:include page="/jsp/admin/includes/menu_common.jsp" />

	</div>
</body>
</html>