<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/jsp/common/includes/set_language.jsp"%><html>
<%@ taglib prefix="ctg" uri="customtags"%>

<body>
	<div class="left_content">
		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="menu.catalogue" />
		</div>

		<div class="med_catalogue">

			<form action="Controller" method="post">
				<input type="hidden" name="param" value="add_med_page" /> <a><input
					type="submit" value="<fmt:message key="button.add_med" />" /></a>
			</form>

			<p class="details">${tryagain}${meddeleted}</p>

			<table class="cart_table">
				<tr class="cart_title">

					<td><fmt:message key="info.table_med_name" /></td>
					<td><fmt:message key="info.table_act_subst" /></td>
					<td><fmt:message key="info.table_med_form" /></td>
					<td><fmt:message key="info.table_price" /></td>
					<td><fmt:message key="info.table_presc_req" /></td>
					<td></td>
				</tr>
				<c:forEach var="medicine" items="${medicines}">
					<tr>

						<ctg:medtab name="${medicine.name}"
							actSubst="${medicine.actSubst}" medForm="${medicine.medForm}"
							price="${medicine.price}" prescReq="${medicine.prescReq}" />
						<td>
							<form action="Controller" method="post">
								<input type="hidden" name="param" value="delete_med" /> <input
									type="hidden" name="med_id" value="${medicine.id}" /> <a><input
									type="submit" value="<fmt:message key="button.delete" />" /></a>
							</form>
						</td>
					</tr>
				</c:forEach>
			</table>

			<table class="num_table">
				<tr>
					<c:forEach begin="1" end="${pages}" varStatus="loop">
						<td><form action="Controller" method="post">
								<input type="hidden" name="param" value="admin_med_cat" /> <input
									type="hidden" name="num" value="${loop.index}" /> <input
									type="submit" value="${loop.index}" />
							</form></td>
					</c:forEach>
				</tr>
			</table>

		</div>

		<div class="clear"></div>

	</div>






</body>
</html>