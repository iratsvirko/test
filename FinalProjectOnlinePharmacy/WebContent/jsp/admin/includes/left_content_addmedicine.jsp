<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%><html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<body>
	<div class="left_content">
		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="label.addmedicine" />
		</div>
		<div class="clear"></div>

		<div class="feat_prod_box_details">

			<div class="contact_form">
				<div class="form_subtitle">
					<fmt:message key="label.addmedicine" />
				</div>

				<div class="form_row">${unavailable}${medexists}</div>

				<form action="Controller" method="post" id="addmed"
					>


					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.med_name" /></strong></label> <input type="text"
							class="contact_input" name="name" />
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.act_subst" /></strong></label>
									 <input type="text" class="contact_input" name="actSubst" />
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.med_form" /></strong></label> 
								<select name="medForm" multiple form="addmed">
						<option selected="selected" >Таблетки</option>
						<option>Капсулы</option>
						<option>Порошок</option>
						<option>Драже</option>
						<option>Капли</option>
						<option>Мазь</option>
						<option>Раствор</option>
						<option>Сироп</option>
					</select>
					</div>
			
					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.price" /></strong></label> 
									<input type="text" class="contact_input"
							name="price" value =0.00 />
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.presc_req" /></strong></label> 
									
									
							<input type="radio" checked  name="prescReq" value = false /> No
							<input type="radio" name="prescReq" value = true /> Yes
							
					</div>


					<input type="hidden" name="param" value="add_med" />
					<div class="form_row">
						<input type="submit" class="register"
							value=<fmt:message key="button.request" /> onclick="check_form()"/>
					</div>
				</form>


			</div>

		</div>


		<div class="clear"></div>
	</div>
</body>
</html>