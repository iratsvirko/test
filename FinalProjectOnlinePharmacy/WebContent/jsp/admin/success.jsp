<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%@include file="/jsp/common/includes/set_language.jsp"%>
<%@include file="/jsp/common/includes/header.jsp"%>
<body>
	<div id="wrap">
	<jsp:include page="/jsp/admin/includes/head_content.jsp" />
		<div class="center_content">
			<jsp:include page="/jsp/admin/includes/left_content_success.jsp" />
			<jsp:include page="/jsp/common/includes/right_content.jsp" />
			<div class="clear"></div>
		</div>
		<jsp:include page="/jsp/common/includes/footer.jsp" />
	</div>



</body>
</html>