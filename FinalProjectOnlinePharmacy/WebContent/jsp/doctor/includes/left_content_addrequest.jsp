<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%><html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<body>
	<div class="left_content">
		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="label.addrequest_doc" />
		</div>
		<div class="clear"></div>

		<div class="feat_prod_box_details">
			<p class="details">
				<fmt:message key="info.addrequest_doc" />
			</p>
			<div class="form_row">${dontexist}${checkdate}${unavailable}</div>

			<div class="contact_form">
				<div class="form_subtitle">
					<fmt:message key="label.addrequest_doc" />
				</div>

				<form action="Controller" method="post">

					<div class="form_row">

						<label class="contact"><strong><fmt:message
									key="label.medicine" /></strong></label>
						<p>${prescription.medName}
						<p>
					</div>


					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.patientFirstName" /></strong></label>
						<p>${prescription.patientFirstName}
						<p>
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.patientLastName" /></strong></label>
						<p>${prescription.patientLastName}
						<p>
					</div>


					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.docName" /></strong></label>
						<p>${prescription.doctorName}
						<p>
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.quantity" /></strong></label>
						<p>${prescription.quantity}
						<p>
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.date" /></strong></label>
						<p>${prescription.date}
						<p>
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.comment" /></strong></label>
						<p>${request.comment}
						<p>
					</div>


					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.valid_date" /></strong></label> <input type="text"
							class="contact_input" name="new_date" value="YYYY-MM-DD" />
					</div>

					<input type="hidden" name="param" value="extend_presc" />
					<div class="form_row">
						<input type="submit" class="register"
							value=<fmt:message key="button.request" /> onclick="check_form()" />
					</div>
				</form>

				<form action="Controller" method="post" class="form_delete">
					<input type="hidden" name="param" value="delete_request" /> <input
						type="submit" class="register"
						value="<fmt:message key="button.delete" />" />
				</form>
			</div>

		</div>


		<div class="clear"></div>
	</div>
</body>
</html>