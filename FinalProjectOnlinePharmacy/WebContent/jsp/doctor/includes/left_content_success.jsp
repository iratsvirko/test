<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%><html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<body>
	<div class="left_content">
		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="menu.catalogue" />
		</div>
		<div class="form_row">${reqdeleted} ${prescadded}${reqextended}${prescdeleted}</div>
		<div class="clear"></div>
	</div>
</body>

</html>