<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="/jsp/common/includes/set_language.jsp"%><html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>

<body>
	<div class="left_content">
		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="label.request" />

		</div>

		<div class="med_catalogue">

			<div class="form_row">${unavailable}</div>


			<table class="cart_table">
				<tr class="cart_title">

					<td><fmt:message key="info.table_presc_id" /></td>
					<td><fmt:message key="label.comment" /></td>
					<td></td>
				</tr>
				<c:forEach var="request" items="${requests}">
					<tr>
						<ctg:reqtab prescNumber="${request.prescNumber}"
							comment="${request.comment}" />

						<td>
							<form action="Controller" method="post">
								<input type="hidden" name="param" value="show_presc" /> <input
									type="hidden" name="request_presc"
									value="${request.prescNumber}" /> <input type="hidden"
									name="request_id" value="${request.id}" /> <a> <input
									type="submit" value="<fmt:message key="button.details" />" /></a>
							</form>
						</td>


					</tr>
				</c:forEach>
			</table>


		</div>

		<div class="clear"></div>

	</div>

</body>
</html>