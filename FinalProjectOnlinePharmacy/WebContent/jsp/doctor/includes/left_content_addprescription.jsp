<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%><html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<body>
	<div class="left_content">
		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="label.addprescription" />
		</div>
		<div class="clear"></div>

		<div class="feat_prod_box_details">
			<p class="details">
				<fmt:message key="info.addprescription" />
			</p>

			<div class="form_row">${prescexists}${nopatient}${checkdate}${unavailable}</div>


			<div class="contact_form">
				<div class="form_subtitle">
					<fmt:message key="label.addprescription" />
				</div>

				<form action="Controller" method="post">

					<input type="hidden" name="param" value="add_presc" />

					<div class="form_row">

						<label class="contact"><strong><fmt:message
									key="label.medicine" /></strong></label>
						<p>${medicine.name}
						<p>
					</div>



					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.docName" /></strong></label>
						<p>${doctor.name}
						<p>
					</div>

					<div class="form_row">

						<label class="contact"><strong><fmt:message
									key="label.patientFirstName" /></strong></label> <input type="text"
							class="contact_input" name="patientFirstName" />
					</div>

					<div class="form_row">

						<label class="contact"><strong><fmt:message
									key="label.patientLastName" /></strong></label> <input type="text"
							class="contact_input" name="patientLastName" />
					</div>


					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.quantity" /></strong></label> <input type="text"
							class="contact_input" name="quantity" value=0 />
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.date" /></strong></label> <input type="text" class="contact_input"
							name="date" value = "YYYY-MM-DD"/>
					</div>


					<div class="form_row">
						<input type="submit" class="register"
							value=<fmt:message key="button.add" /> onclick="check_form()"/>
					</div>
				</form>
			</div>
		</div>


		<div class="clear"></div>
	</div>
</body>
</html>