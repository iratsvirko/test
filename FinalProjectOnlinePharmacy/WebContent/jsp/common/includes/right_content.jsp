<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<body>
	<div class="right_content">
	
		<div class="languages_box">


			<span><fmt:message key="message.language" /></span>
			
			<form action="Controller" method="post">
				<input type="hidden" name="param" value="language" /> <input
					type="hidden" name="language" value="en" />
					<a> <input type="image"
					src="images/gb.gif" alt=<fmt:message key="menu.main"/> ></a>
			</form>

			<form action="Controller" method="post">
				<input type="hidden" name="param" value="language" /> <input
					type="hidden" name="language" value="ru" />
					<a> <input type="image"
					src="images/ru.gif" alt=<fmt:message key="menu.main"/>  ></a>
			</form>


		</div>


		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="message.welcome" />
		</div>
		<div class="about">
			<p>
				<fmt:message key="info.welcome_info_a" />
			</p>
			<p>
				<fmt:message key="info.welcome_info_b" />
			</p>
			<ul class="you_can">
				<li><fmt:message key="info.you_can_a" /></li>
				<li><fmt:message key="info.you_can_b" /></li>
				<li><fmt:message key="info.you_can_c" /></li>
				<li><fmt:message key="info.you_can_d" /></li>
			</ul>
			<p>
				<fmt:message key="info.you_can_e" />
			</p>
			<p>
				<fmt:message key="info.you_can_f" />
			</p>

		</div>
	</div>
</body>
</html>