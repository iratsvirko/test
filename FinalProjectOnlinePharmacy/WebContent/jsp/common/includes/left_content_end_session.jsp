<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@include file="/jsp/common/includes/set_language.jsp"%>
<html>

<body>
	<div class="left_content">


		<div class="form_row">
			
			<fmt:message key="message.session_end" />
			
		</div>



		<div class="clear"></div>
	</div>
</body>
</html>