<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%><html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<body>
	<div class="left_content">
		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="label.mainlogin" />
		</div>
		<div class="clear"></div>

		<div class="feat_prod_box_details">
			<p class="details">
				<fmt:message key="info.login" />
			</p>
			<div class="form_row">${checklogin}${fillall}</div>

			<div class="contact_form">
				<div class="form_subtitle">
					<fmt:message key="message.login_input" />
				</div>


				<form action="Controller" method="post">
					<input type="hidden" name="param" value="login" />

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.login" /></strong></label> <input type="email" class="contact_input"
							name="email" />
					</div>


					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.password" /></strong></label> <input type="password"
							class="contact_input" name="password" />
					</div>

					<div class="form_row">
						<input type="submit" class="register" onclick="check_form()"
							value=<fmt:message key="button.login" /> />
					</div>



				</form>
			</div>

		</div>


		<div class="clear"></div>
	</div>
</body>
</html>