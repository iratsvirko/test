<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>

<body>
	<div id="menu">
		<ul>

			<li>
				<form action="Controller" method="post">
					<input type="hidden" name="param" value="common_home" /> <a> <input
						type="submit" value="<fmt:message key="menu.main" />" />
					</a>
				</form>
			</li>
			<li>
				<form action="Controller" method="post">
					<input type="hidden" name="param" value="guest_med_cat" /> <input
						type="hidden" name="num" value="1" /> <a><input type="submit"
						value="<fmt:message key="menu.catalogue" />" /> </a>
				</form>
			</li>
			<li>
				<form action="Controller" method="post">
					<input type="hidden" name="param" value="register" /> <a><input
						type="submit" value="<fmt:message key="menu.register" />" /></a>
				</form>
			</li>
			<li>
				<form action="Controller" method="post">
					<input type="hidden" name="param" value="login_page" /> <a><input
						type="submit" value="<fmt:message key="menu.login" />" /></a>
				</form>
			</li>
		</ul>
	</div>





</body>
</html>