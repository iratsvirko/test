<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%><html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<body>
	<div class="left_content">
		<div class="title">
			<span class="title_icon"></span>
			<fmt:message key="label.register" />
		</div>
		<div class="clear"></div>

		<div class="feat_prod_box_details">
			<p class="details">
				<fmt:message key="info.register" />
			</p>
			<div class="form_row">${emailexists}</div>

			<div class="contact_form">
				<div class="form_subtitle">
					<fmt:message key="message.register_input" />
				</div>


				<form action="Controller" method="post">
					<input type="hidden" name="param" value="regpatient" />

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.login" /></strong></label> <input id="valid" type="text"
							class="contact_input" name="email" />
					</div>


					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.password" /></strong></label> <input type="text"
							value="From 6 to 10 characters" class="contact_input"
							name="password" />
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.firstName" /></strong></label> <input type="text"
							class="contact_input" name="firstName" />
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.lastName" /></strong></label> <input type="text"
							class="contact_input" name="lastName" />
					</div>



					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.phoneNumber" /></strong></label> <input type="text"
							value="(29/25/44/33)xxxxxxx" class="contact_input"
							name="phoneNumber" />
					</div>

					<div class="form_row">
						<label class="contact"><strong><fmt:message
									key="label.cardNumber" /></strong></label> <input type="text"
							value="xxxxxxxxxxxxxxxx" class="contact_input" name="cardNumber" />
					</div>


					<div class="form_row">
						<input type="submit" class="register" onclick="check_form()"
							value=<fmt:message key="button.registration" /> />
					</div>





				</form>
			</div>

		</div>


		<div class="clear"></div>
	</div>
</body>
</html>