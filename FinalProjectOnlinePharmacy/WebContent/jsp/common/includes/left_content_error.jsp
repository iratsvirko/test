<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>

<body>
	<div class="left_content">

		
			<div id="container">
				<br />
				<fmt:message key="message.error" />
				<br />
				<fmt:message key="message.stcode" />${pageContext.errorData.statusCode}
				<br />
				<fmt:message key="message.exception" />${pageContext.exception}
				<br />
			</div>
		


		<div class="clear"></div>
	</div>
</body>
</html>