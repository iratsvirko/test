<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>

<body>
       	<div class="left_content">

          <div class="title"><span class="title_icon"></span><fmt:message key="message.popular" /></div>

        	<div class="feat_prod_box">

            	<div class="prod_img"><img src="images/prod1.png" border="0" /></div>

                <div class="prod_det_box">
                	<div class="box_top"></div>
                    <div class="box_center">
                    <div class="prod_title"><fmt:message key="heading.merz"/></div>
                    <p class="details"><fmt:message key="info.merz"/></p>
                      <ul class = "merz">
                        <li><fmt:message key="info.merz_li_a"/></li>
                        <li><fmt:message key="info.merz_li_b"/></li>
                        <li><fmt:message key="info.merz_li_c"/></li>
                        <li><fmt:message key="info.merz_li_d"/></li>
                      </ul>
                    <div class="clear"></div>
                    </div>
                    <div class="box_bottom"></div>
                </div>
            <div class="clear"></div>

            <div class="prod_img"><img src="images/prod2.jpg"border="0" /></div>

              <div class="prod_det_box">
                <div class="box_top"></div>
                  <div class="box_center">
                  <div class="prod_title"><fmt:message key="heading.persen"/></div>
                  <p class="details"><fmt:message key="info.persen_a"/></p>
                  <p class="details"><fmt:message key="info.persen_b"/></p>
                  <div class="clear"></div>
                  </div>
                  <div class="box_bottom"></div>
              </div>
            </div>
        <div class="clear"></div>
        </div>
</body>
</html>