<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="message.site_title" /></title>
<link rel="stylesheet" type="text/css" href="./css/style.css">
</head>