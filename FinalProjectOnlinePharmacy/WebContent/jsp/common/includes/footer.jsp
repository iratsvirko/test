<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@include file="/jsp/common/includes/set_language.jsp"%>
<html>
<body>
	<div class="footer">
			<div class="left_footer">
				<img src="images/footer_logo.gif" /><br />
			</div>
			<div class="right_footer">
				<a href="#"><fmt:message key="message.up" /></a>
			</div>
		</div>
</body>
</html>