<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/jsp/common/includes/set_language.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>

<body>
	<div class="head_content">
		<form action="Controller" method="post">
			<input type="hidden" name="param" value="common_home" /> 
			<input type="image" src="images/logo.gif" alt=<fmt:message key="menu.main"/> class="logo">
		</form>

		<jsp:include page="/jsp/common/includes/menu_common.jsp" />

	</div>
</body>
</html>