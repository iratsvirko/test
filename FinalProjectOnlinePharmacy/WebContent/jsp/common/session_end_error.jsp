<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isErrorPage="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<%@include file="/jsp/common/includes/set_language.jsp"%>
<%@include file="/jsp/common/includes/header.jsp"%>

<body>
	<div id="wrap">
	<jsp:include page="/jsp/common/includes/head_content.jsp" />
		<div class="center_content">
			<jsp:include page="/jsp/common/includes/left_content_end_session.jsp" />
			<jsp:include page="/jsp/common/includes/right_content.jsp" />
			<div class="clear"></div>
		</div>
		<jsp:include page="/jsp/common/includes/footer.jsp" />
	</div>



</body>

</html>