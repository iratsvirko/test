function validateInput(element) {
	var name = element.getAttribute('name'), validate = false;

	switch (name) {
	case "email": {
		reg = /(^([a-z0-9_-]+\.)*[a-z0-9_-]+)(@)([a-z0-9_-]+)(\.)([a-z]{2,6})$/;
		if (reg.test(element.value) === true) {
			validate = true;
		}
		break;
	}
	case "password":

		reg = /(^\w{3,10}$)/;
		if (reg.test(element.value) === true) {
			validate = true;
		}
		break;
	case "firstName":
	case "lastName":
	case "patientLastName":
	case "patientFirstName":
	case "name":
	case "actSubst":
		reg = /(^[A-ZА-Яа-яёa-z- ]{2,20}$)/

		if (reg.test(element.value) === true) {
			validate = true;
		}
		break;

	case "price":

		reg = /^(?=.*[1-9])\d+(?:\.\d{1,2})?$/;
		if (reg.test(element.value) === true) {
			validate = true;
		}
		break;
		
	case "phoneNumber": {
		reg = /(29|44|25|33)([1-9]{1})(\d{6})/;
		if (reg.test(element.value) === true) {
			validate = true;
		}
		break;
	}
	case "cardNumber":

		reg = /(^[0-9]{16}$)/;
		if (reg.test(element.value) === true) {
			validate = true;
		}
		break;

	case "quantity":

		reg = /(^[1-9]{1,2}$)/;
		if (reg.test(element.value) === true) {
			validate = true;
		}
		break;

	case "comment":

		if (element.value.length > 3 && element.value.length < 140
				&& element.value !== "") {
			validate = true;
		}
		break;

	case "medForm":

		if (element.value !== "") {
			validate = true;
		}
		break;

	case "date":
	case "new_date":

		reg = /(201)[6-8]-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/;
		if (reg.test(element.value) === true) {
			validate = true;
		}
		break;

	default:
		validate = true;

	}

	if (validate === false) {
		element.className = "contact_input error";
		return false;
	} else {
		element.className = "contact_input";
		return true;
	}

}

function check_form() {
	var arr = document.getElementsByClassName("contact_input");
	var count=0;
	for (var i = 0; i < arr.length; i++) {
		var a = validateInput(arr[i]);
		if(a==false){
			count++;
		}
	}
	
	if (count!=0) {
		event.preventDefault();
	}
}