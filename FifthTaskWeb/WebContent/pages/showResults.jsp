<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delivery</title>
</head>
<body>
<h1>Old Cards</h1>
<table style="width: 300px" border="1">
		<tr bgcolor="blue">
			<th>Barcode</th>
			<th>Theme</th>
			<th>Type</th>
			<th>Country</th>
			<th>Year</th>
			<th>Author</th>
			<th>Valuable</th>
			<th>Sending Status</th>	
		</tr>
		<c:forEach var="oldcard" items="${oldcard }">
			<tr>
				<td>${oldcard.barcode}</td>
				<td>${oldcard.theme}</td>
				<td>${oldcard.type}</td>
				<td>${oldcard.country}</td>
				<td>${oldcard.year}</td>
				<td>${oldcard.author}</td>
				<td>${oldcard.valuable}</td>
				<td>${oldcard.sendst}</td>		
			</tr>
		</c:forEach>
	</table>



</body>
</html>