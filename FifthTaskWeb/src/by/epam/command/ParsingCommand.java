package by.epam.command;

import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;

import by.epam.entity.OldCard;
import by.epam.exceptions.LogicException;
import by.epam.parsers.AbstractParser;
import by.epam.parsers.DOMParser;
import by.epam.parsers.SaxParser;
import by.epam.parsers.StAXParser;


public class ParsingCommand {
	private static final Logger Log = Logger.getLogger(ParsingCommand.class);
	public static String execute(HttpServletRequest request) {

		
		ResourceBundle urlBundle = ResourceBundle.getBundle("URLBundle");
		String parserType = request.getParameter("parser");
		AbstractParser parser = null;
		List<OldCard> list = null;
		List<OldCard> cardList = new ArrayList<>();
		Log.info(parserType);
		switch (parserType) {
		case "DOM":
			Log.info("DOM");
			parser = new DOMParser();
			break;
		case "SAX":
			Log.info("SAX");
			parser = new SaxParser();
			break;
		case "StAX":
			Log.info("StAX");
			parser = new StAXParser();
			break;
		default:
			break;
		}

		try {
			Log.info(urlBundle.getString("xmlURL"));
			list = parser.getOldCardsList(urlBundle.getString("xmlURL"));

		} catch (LogicException e) {
			Log.info("check the correctness of input data.");
			Log.info(e);
		}
		Log.info(list);
		
		for (OldCard card : list) {
				cardList.add(card);
		}
		request.setAttribute("oldcard", cardList);
				
		return urlBundle.getString("page-result");
	}


}
