package by.epam.parsers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import by.epam.entity.OldCard;

import by.epam.exceptions.LogicException;

public class StAXParser extends AbstractParser {

	@Override
	public List<OldCard> getOldCardsList(String url) throws LogicException {
		List<OldCard> cardsList = new ArrayList<>();
		OldCard currCard = null;
		String tagContent = null;

		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader reader = null;
		try {
			reader = factory.createXMLStreamReader(new FileInputStream(url));
		} catch (FileNotFoundException e) {
			throw new LogicException("file not found exception.", e);
		} catch (XMLStreamException e) {
			throw new LogicException("xml stream exception.", e);
		}

		if (reader != null) {
			try {
				while (reader.hasNext()) {
					int event = reader.next();
					switch (event) {
					case XMLStreamConstants.START_ELEMENT:

						if ("oldcard".equals(reader.getLocalName())) {
							currCard = new OldCard();
						}
						break;
					case XMLStreamConstants.CHARACTERS:
						tagContent = reader.getText().trim();
						break;
					case XMLStreamConstants.END_ELEMENT:
						if ("oldcard".equals(reader.getLocalName())) {
							cardsList.add(currCard);
						}
						if ("barcode".equals(reader.getLocalName())) {
							currCard.setBarcode(tagContent);
						}
						if ("theme".equals(reader.getLocalName())) {
							currCard.setTheme(tagContent);
						}
						if ("type".equals(reader.getLocalName())) {
							currCard.setType(tagContent);
						}
						if ("country".equals(reader.getLocalName())) {
							currCard.setCountry(tagContent);
						}
						if ("year".equals(reader.getLocalName())) {
							currCard.setYear(Integer.parseInt(tagContent));
						}
						if ("author".equals(reader.getLocalName())) {
							currCard.setAuthor(tagContent);
						}if ("valuable".equals(reader.getLocalName())) {
							currCard.setValuable(tagContent);
						}
						
						if ("sendingstatus".equals(reader.getLocalName())) {
							currCard.setSendst(Boolean.parseBoolean(tagContent));
						}
						break;
					case XMLStreamConstants.START_DOCUMENT:
						cardsList = new ArrayList<>();
						break;
					}
				}
			} catch (NumberFormatException e) {
				throw new LogicException("number format exception", e);
			} catch (XMLStreamException e) {
				throw new LogicException("xml stream exception", e);
			}
		} else {
			throw new LogicException("reader exception.");
		}

		return cardsList;
	}


}
