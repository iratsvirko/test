package by.epam.parsers;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import by.epam.entity.OldCard;


public class SaxHandler extends DefaultHandler {
	private List<OldCard> cards = null;
	private OldCard card = null;

	public List<OldCard> getOldCards() {
		return cards;
	}

	boolean bBarcode = false;
	boolean bTheme = false;
	boolean bType = false;
	boolean bCountry = false;
	boolean bYear = false;
	boolean bAuthor = false;
	boolean bValuable = false;
	boolean bSendSt = false;
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		switch (qName) {
		case "oldcard":
			card = new OldCard();
			if (cards == null) {
				cards = new ArrayList<>();
			}
			break;

		case "barcode":
			bBarcode = true;
			break;
		case "theme":
			bTheme = true;
			break;
		case "type":
			bType = true;
			break;
		case "country":
			bCountry = true;
			break;
		case "year":
			bYear = true;
			break;
		case "author":
			bAuthor = true;
			break;
		case "valuable":
			bValuable = true;
			break;
		case "sendingstatus":
			bSendSt = true;
			break;
		}

	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (bBarcode) {
			card.setBarcode(new String(ch, start, length));
			bBarcode = false;
		} else if (bTheme) {
			card.setTheme(new String(ch, start, length));
			bTheme = false;
		} else if (bType) {
			card.setType(new String(ch, start, length));
			bType = false;
		} else if (bCountry) {
			card.setCountry(new String(ch, start, length));
			bCountry = false;
		} else if (bYear) {
			card.setYear(Integer.parseInt(new String(ch, start, length)));
			bYear = false;
		} else if (bAuthor) {
			card.setAuthor(new String(ch, start, length));
			bAuthor = false;
		} else if (bValuable) {
			card.setValuable(new String(ch, start, length));
			bValuable = false;
		}else if (bSendSt) {
			card.setSendst(Boolean.parseBoolean(new String(ch, start, length)));;
			bValuable = false;
		}

	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if ("oldcard".equalsIgnoreCase(qName)) {
			cards.add(card);
		}
	}

	} 

