package by.epam.parsers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import by.epam.entity.OldCard;
import by.epam.exceptions.LogicException;

public class SaxParser extends AbstractParser{

	@Override
	public List<OldCard> getOldCardsList(String url) throws LogicException {
		
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		List<OldCard> list = null;
				try {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			DefaultHandler handler = new SaxHandler();
			saxParser.parse(new File(url), handler);

			list = ((SaxHandler)handler).getOldCards();

		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw new LogicException(e);
		}
		return list;
	}
}

