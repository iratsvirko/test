package com.irats.informationhandling.util;

import java.util.Comparator;
import java.util.Arrays;

public class  SortByLetterMeetings implements Comparator<String> {
		private char letter;

		public SortByLetterMeetings(char l) {
			letter = l;
		}

		int matchesCount(String s) {
			int found = 0;

			for (char c :  s.toCharArray())
				if (letter == c)
					++found;

			return found;
		}

		public int compare(String a, String b) {
			int diff = matchesCount(a) - matchesCount(b);
			return (diff != 0) ? diff : a.compareTo(b);
		}
	}

	
