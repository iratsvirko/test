package com.irats.informationhandling.entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class SymbolParser {
	public static final String REGEX_SYMBOL = ".{1}";
	public static final Logger Log = Logger.getLogger(SymbolParser.class);
	public static Text parseToSymbol(Text wordSignList, String wordSign) {
		Pattern pattern = Pattern.compile(REGEX_SYMBOL);
		String symbol = "";
		Matcher matcher = pattern.matcher(wordSign);
		TextElement symbolList;
		while (matcher.find()) {
			symbol = matcher.group();
			symbolList = new TextElement(symbol);
			
		System.out.print(symbolList.toString());
		}
		return wordSignList;
	}

}
