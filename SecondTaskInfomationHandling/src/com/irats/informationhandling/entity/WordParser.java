package com.irats.informationhandling.entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordParser {
	
	public static final String REGEX_WORD = "([^(\\s)]*)(\\s)*";

	public static Text parseToWord(Text sentenceList, String sentence) {
		Pattern patternWord = Pattern.compile(REGEX_WORD);
		String word = "";
		Matcher matcher = patternWord.matcher(sentence);
		Text wordList = new Text();
		while (matcher.find()) {
			word = matcher.group();
			wordList = SignAndWordParser.parseToSignAndWord(wordList, word);
			sentenceList.addElement(wordList);
		}
		return sentenceList;

	}

}
