package com.irats.informationhandling.entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignAndWordParser {
	
	public static final String REGEX_WORD_AND_SIGN = "([\\.,!\\?:;@]{1})|([^\\.,!\\?:;@]*)";

	public static Text parseToSignAndWord(Text wordList, String word) {
		
		Pattern pattern = Pattern.compile(REGEX_WORD_AND_SIGN);
		String wordSign = "";
		Matcher matcher = pattern.matcher(word);
		Text wordSignList = new Text();
		while (matcher.find()) {
			wordSign = matcher.group();
			wordSignList = SymbolParser.parseToSymbol(wordSignList, wordSign);
			wordList.addElement(wordSignList);
			
		}
		return wordList;
		
	}

}
