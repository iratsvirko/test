package com.irats.informationhandling.entity;

import java.util.LinkedList;
import java.util.List;

public class Text implements Devided {

	private List<Devided> parts = new LinkedList<Devided>();

	@Override
	public String toString() {
		return parts.toString();
	}

	@Override
	public void addElement(Devided type) {
		parts.add(type);

	}

	@Override
	public void removeElement(Devided type) {
		parts.remove(type);

	}

	@Override
	public Devided getElement(int index) {

		return parts.get(index);
	}

	public List<Devided> getParts() {
		return parts;
	}

}
