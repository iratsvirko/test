package com.irats.informationhandling.entity;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TextInitialization {
	static String text = "";
	public static String initialization(final String path) {

		try {
			FileInputStream inFile = new FileInputStream(path);
			byte[] str = new byte[inFile.available()];
			inFile.read(str);
			text = new String(str);
			inFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return text;
	}
	@Override
	public String toString() {
		return text;
	}

}
