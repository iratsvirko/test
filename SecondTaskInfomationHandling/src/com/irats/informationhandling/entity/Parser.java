package com.irats.informationhandling.entity;

public class Parser {
	public static Text parse(String path){ 
	String text = TextInitialization.initialization(path);
	Text wholeText=new Text();
	wholeText= ParagraphParser.parseToParagraph(wholeText, text);
	//System.out.println(wholeText);
	return wholeText;

	}
	
}
