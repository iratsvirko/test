package com.irats.informationhandling.entity;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.irats.informationhandling.main.Runner;

public class ParagraphParser {
	public static final String REGEX_LISTING = "\\s*(Start listing)([^\\t]+)(End of listing)";
	public static final String REGEX_PARAGRAPH_WITH_LISTING = "(\\s*(.+))([^(\\s*(Start listing)([^\\t]+)(End of listing)\\s)])|\\s*(Start listing)([^\\t]+)(End of listing)";
	public static final Logger Log = Logger.getLogger(ParagraphParser.class);
		
	static public Text parseToParagraph(Text wholeText, String text){
		Text paragraphList = new Text();
		
		Pattern patternParagraph = Pattern.compile(REGEX_PARAGRAPH_WITH_LISTING);
		
		TextElement paragraphPart = null;
		String paragraph = "";
		
		Matcher matcher = patternParagraph.matcher(text);
		
		while (matcher.find()) {
			paragraph = matcher.group();
			if (Pattern.matches(REGEX_LISTING, paragraph)) {
				paragraphPart = new TextElement(paragraph);
				//System.out.println(paragraphPart.toString());
				paragraphList.addElement(paragraphPart);
			} else {
				//System.out.println(paragraph.toString());
				paragraphList = SentenceParser.parseToSentense(paragraphList, paragraph);

			}
			wholeText.addElement(paragraphList);
			
		}
return wholeText;

	}

}
