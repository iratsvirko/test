package com.irats.informationhandling.entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser {
	
	public static final String REGEX_SENTENCE = "((\\d\\.)*(\\d\\.)*[^(\\.|!|\\?)]+)(\\.|!|\\?)";

	public static Text parseToSentense(Text paragraphList, String paragraph) {
		
		Pattern patternSentense = Pattern.compile(REGEX_SENTENCE);
		Matcher matcher = patternSentense.matcher(paragraph);
		String sentence = "";
		Text sentenceList = new Text();
		while (matcher.find()) {
			sentence = matcher.group();
			//System.out.println(sentence.toString());
			sentenceList = WordParser.parseToWord(sentenceList, sentence);
			paragraphList.addElement(sentenceList);
			
		}
		return paragraphList;
	}

}
