package com.irats.multithreading.entity;

import java.util.Random;
import java.util.concurrent.Exchanger;

import org.apache.log4j.Logger;

import com.irats.multithreading.util.ResourceException;

public class Ship extends Thread {
	private static final Logger log = Logger.getLogger(Ship.class);
	private static final int SHIP_CAPACITY = 3;
	private Integer cont = new java.util.Random().nextInt(4);
	private boolean reading = false;
	private DockPool<Dock> pool;
	private Dock dock = null;

	public Ship(DockPool<Dock> pool) {
		this.pool = pool;
	}

	@Override
	public void run() {
		try {
			dock = pool.getDock();
			reading = true;
			log.info("Ship #" + this.getId() + " (" + this.getCont() + " CONT) took Dock #" + dock.getNumber() + " ("
					+ dock.getCont() + " CONT)");

			if (cont == 0) {
				shipLoad();
			} else {
				if (cont < SHIP_CAPACITY && cont != 0) {
					shipUnload();
					shipLoad();
				}
			}
			if (cont == SHIP_CAPACITY) {
				shipUnload();
			}


		}

		catch (ResourceException e) {
			e.printStackTrace();
	
		}

		finally {
			if (dock != null) {
				reading = false;
				log.info("Ship #" + this.getId() + " (" + this.getCont() + " CONT) released Dock #" + dock.getNumber()
						+ " (" + dock.getCont() + " CONT)");
				pool.returnDock(dock);
			}
		}

	}

	public void shipLoad() {

		log.info("Ship #" + this.getId() + " is loading...");
		try {
			while (cont < Ship.SHIP_CAPACITY && dock.loadFromStock() != 0)
						{
				cont = cont + dock.getCont();
				dock.loadFromStock();
				sleep(100);
			}

		} catch (InterruptedException e) {
			log.info("Ship #" + this.getId() + " has problems with loading");
		}

	}

	public void shipUnload() {
		log.info("Ship #" + this.getId() + " is unloading...");
		try {
			while (cont > 0) {
				cont = cont - dock.unloadOnStock();
				sleep(100);
			}

		} catch (InterruptedException e) {
			log.info("Ship #" + this.getId() + " has problems with loading");

		}
	}

	public int getCont() {
		return cont;
	}

	public void setCont(int cont) {
		this.cont = cont;
	}

	public static int getCapacity() {
		return SHIP_CAPACITY;
	}

	public boolean isReading() {
		return reading;
	}

}