package com.irats.multithreading.entity;

import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import com.irats.multithreading.util.ResourceException;

import java.util.LinkedList;

public class DockPool<T> {
	private final static int POOL_SIZE = 4;
	private final Semaphore semaphore = new Semaphore(POOL_SIZE, true);
	private final Queue<T> dockQueue = new LinkedList<T>();

	public DockPool(Queue<T> docks) {
		dockQueue.addAll(docks);
	}

	public T getDock() throws ResourceException {
		try {
			semaphore.acquire();
			T dock = dockQueue.poll();
			return dock;

		} catch (InterruptedException e) {
			throw new ResourceException(e);
		}
	}

	public void returnDock(T dock) {
		dockQueue.add(dock);
		semaphore.release();
	}
}
