package com.irats.multithreading.entity;

import org.apache.log4j.Logger;

public class Dock {
	private static final Logger log = Logger.getLogger(Dock.class);
	private int number;
	private static final int STOCK_CAPACITY = 10;
	private int cont=new java.util.Random().nextInt(4);

	public Dock(int number) {
		this.number = number;
	}
	
	public int unloadOnStock() {
		
		while (cont < STOCK_CAPACITY) {
			cont++;
			log.info("Dock #" + this.getNumber() + "(" + cont + " CONT after ship unloading)");
			return 1;
		}
	
	return 0;
}

public int loadFromStock() {
	while (cont > 0) {
		cont--;
		log.info("Dock #" + this.getNumber() + "(" +  cont + " CONT after ship loading");
		return 1;
	}
	return 0;
}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getCont() {
		return cont;
	}

	public void setCont(int cont) {
		this.cont = cont;
	}

	
}

