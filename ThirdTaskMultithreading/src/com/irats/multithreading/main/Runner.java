package com.irats.multithreading.main;

import java.util.LinkedList;

import com.irats.multithreading.entity.Dock;
import com.irats.multithreading.entity.DockPool;
import com.irats.multithreading.entity.Ship;


public class Runner {

	public static void main(String[] args) {
		LinkedList<Dock> list = new LinkedList<Dock>() {
			
			{
				this.add(new Dock(1));
				this.add(new Dock(2));
				this.add(new Dock(3));
				this.add(new Dock(4));
			}
		};
		DockPool<Dock> pool = new DockPool<>(list);
		for (int i = 0; i < 10; i++) {
			new Ship(pool).start();
		}

	}
}
