package com.irats.musichierarchy.entity;

import java.util.Arrays;

import org.apache.log4j.Logger;

public class Podcast extends Track implements Playable {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(Podcast.class);

	/** The another artist. */
	private String[] anotherArtist;
	
	/**
	 * Instantiates a new podcast.
	 *
	 * @param name the name
	 * @param genre the genre
	 * @param duration the duration
	 * @param artist the artist
	 * @param anotherArtist the another artist
	 */
	public Podcast(String name, String genre, int duration, String artist, String... anotherArtist) {
		super(name, artist, genre, duration);
		this.anotherArtist = anotherArtist;
	}

	/**
	 * My playlist.
	 */
	public void myPlaylist() {
		LOGGER.info("Now I'm listening to this podcast!");
	};

	/**
	 * Gets the another artist.
	 *
	 * @return the another artist
	 */
	public String[] getAnotherArtist() {
		return anotherArtist;
	}

	@Override
	public String toString() {
		return "\n" + "Podcast [name: " + getName() + "; duration: " + getDuration() + " sec;" + " genre: " + getGenre()
				+ " artist: " + getArtist() + Arrays.toString(anotherArtist) + "]";
	}

}
