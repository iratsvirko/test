package com.irats.musichierarchy.entity;

import org.apache.log4j.Logger;

/**
 * The Class Sample.
 */
public class Sample extends Track {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(Podcast.class);

	/**
	 * Instantiates a new sample.
	 *
	 * @param name the name
	 * @param artist the artist
	 * @param genre the genre
	 * @param duration the duration
	 */
	public Sample(String name, String artist, String genre, int duration) {
		super(name, artist, genre, duration = 30);
		;
	}
	/**
	 * My ringtone.
	 */
	public void myRingtone() {
		LOGGER.info("Now this sample is my ringtone!");
	};

	@Override
	public String toString() {
		return "\n" + "Sample [name: " + getName() + " artist: " + getArtist() + "; duration: " + getDuration()
				+ " sec;" + " genre: " + getGenre() + "]";
	};

}
