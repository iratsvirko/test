package com.irats.musichierarchy.entity;

/**
 * The Class AlbumTrack.
 */
public class AlbumTrack extends Track implements Playable {

	/** The album name. */
	private String albumName;
	/** The album year. */
	private int albumYear;
	/** The another artist. */
	private String[] anotherArtist;
	/**
	 * Instantiates a new album track.
	 *
	 * @param name
	 *            the name
	 * @param artist
	 *            the artist
	 * @param duration
	 *            the duration
	 * @param genre
	 *            the genre
	 * @param albumName
	 *            the album name
	 * @param albumYear
	 *            the album year
	 */
	public AlbumTrack(String name, String artist, int duration, String genre, String albumName, int albumYear) {
		super(name, artist, genre, duration);
		this.albumName = albumName;
		this.albumYear = albumYear;

	}

	/**
	 * Gets the album name.
	 *
	 * @return the album name
	 */
	public String getAlbumName() {
		return albumName;
	}
	/**
	 * Gets the album year.
	 *
	 * @return the album year
	 */
	public int getAlbumYear() {
		return albumYear;
	}
	/**
	 * Gets the another artist.
	 *
	 * @return the another artist
	 */
	public String[] getAnotherArtist() {
		return anotherArtist;
	}

	@Override
	public String toString() {
		return "\n" + "AlbumTrack [name: " + getName() + " artist: " + getArtist() + "; duration: " + getDuration()
				+ " sec;" + " genre: " + getGenre() + "; albumName: " + getAlbumName() + "; albumYear: "
				+ getAlbumYear() + "]";

	};

}
