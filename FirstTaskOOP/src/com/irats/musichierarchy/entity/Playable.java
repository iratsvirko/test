package com.irats.musichierarchy.entity;

public interface Playable {
	void play ();
	void pause ();
	void stop ();

}
