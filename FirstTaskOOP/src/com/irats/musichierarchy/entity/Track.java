package com.irats.musichierarchy.entity;

import org.apache.log4j.Logger;

import com.irats.musichierarchy.main.Runner;

/**
 * The Class Track.
 */
public abstract class Track implements Playable {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(Runner.class);
	
	/** The name. */
	private String name;
	
	/** The artist. */
	private String artist;
	
	/** The genre. */
	private String genre;
	
	/** The duration. */
	private int duration;	


	/**
	 * Instantiates a new track.
	 *
	 * @param name the name
	 * @param artist the artist
	 * @param genre the genre
	 * @param duration the duration
	 */
	Track(String name, String artist, String genre, int duration) {
		this.name = name;
		this.genre = genre;
		this.artist = artist;
		this.duration = duration;

	}
public void play() {
		LOGGER.info("Sound");
	}
	
	public void pause() {
		LOGGER.info("Pause");
	}

	public void stop() {
		LOGGER.info("Stop");
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the artist.
	 *
	 * @return the artist
	 */
	public String getArtist() {
		return artist;
	}

	/**
	 * Gets the genre.
	 *
	 * @return the genre
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * Gets the duration.
	 *
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

}
