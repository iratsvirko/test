package com.irats.musichierarchy.util;

import java.util.Comparator;

import com.irats.musichierarchy.entity.Track;

public class GenreReplacement implements Comparator<Track>{

	@Override
	public int compare(Track a, Track b) {
		return a.getGenre().compareTo(b.getGenre());
	}
	
}
