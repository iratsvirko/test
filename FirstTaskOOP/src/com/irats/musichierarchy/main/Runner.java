package com.irats.musichierarchy.main;

import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.log4j.Logger;

import com.irats.musichierarchy.entity.AlbumTrack;
import com.irats.musichierarchy.entity.Podcast;
import com.irats.musichierarchy.entity.Sample;
import com.irats.musichierarchy.entity.Track;
import com.irats.musichierarchy.util.GenreReplacement;

/**
 * The Class Runner.
 */
public class Runner {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(Runner.class);

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

		AlbumTrack track1 = new AlbumTrack("Fall", "Imagine Dragons", 200, "Pop-Rock", "Smoke&Mirrors", 2015);
		AlbumTrack track2 = new AlbumTrack("Not afraid", "Eminem", 235, "Hip-hop", "Recovery", 2010);
		AlbumTrack track3 = new AlbumTrack("Centuries", "Fall out boy", 247, "Pop-Rock",
				"American Beauty/American Psyco", 2010);
		AlbumTrack track4 = new AlbumTrack("Across the stars", "John Williams", 301, "Instrumental", "Star Wars OST",
				2008);
		Sample sample1 = new Sample("Cold wind blows", "Eminem", "Hip-hop", 30);
		Sample sample2 = new Sample("99 problems", "Jay-Z", "Hip-hop", 59);
		Sample sample3 = new Sample("Up&Up", "Coldplay", "Pop", 345);
		Sample sample4 = new Sample("Voodoo Child", "Brick+Mortar", "Pop", 45);

		Podcast podcast2 = new Podcast("Intense", "Trance", 7350, "Armin van Burren");

		Podcast podcast1 = new Podcast("Violin", "Instrumental", 7214, "Vanessa Mei", "Lindsie Stirling",
				"David Garrat");

		Podcast podcast3 = new Podcast("Depression", "Pop", 6915, "Lana Del Rey", "Radiohead", "The Weekend");

		LOGGER.info("Starting loading playlist...");

		List<Track> playList = new ArrayList<Track>();

		playList.add(track1);
		playList.add(track2);
		playList.add(track3);
		playList.add(track4);
		playList.add(sample1);
		playList.add(sample2);
		playList.add(sample3);
		playList.add(sample4);
		playList.add(podcast1);
		playList.add(podcast2);
		playList.add(podcast3);


		LOGGER.info("Playlist loaded.");

		int totalDuration = 0;
		for (int i = 0; i < playList.size(); i++) {

			int element = (playList.get(i).getDuration());
			totalDuration += element;
		}
		String formattedDate = new SimpleDateFormat("HH.mm.ss").format(totalDuration);
		
		LOGGER.info("Total duration of playlist is: " + formattedDate);

		Collections.sort(playList, new GenreReplacement());

		LOGGER.info("Collection is sorted by genre.");

		int duration1 = 300;
		int duration2 = 10000;
		for (int i = 0; i < playList.size(); i++) {
			if (playList.get(i).getDuration() >= duration1 && playList.get(i).getDuration() <= duration2) {

			}

		}
		LOGGER.info("Required tracks are found.");
	}

}
