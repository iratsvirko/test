<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" />

	<xsl:template match="/">
		<oldcards>
			<xsl:apply-templates />
		</oldcards>
	</xsl:template>

<xsl:template match="oldcard">
	
	<theme>
		<xsl:value-of select="theme" />
	</theme>

		<type>
		<xsl:value-of select="type" />
		</type>


	<valuable>
		<xsl:value-of select="valuable" />
	</valuable>
	
	<description>
		<xsl:template match="country">
			country="{description/country}"
		</xsl:template>

		<xsl:template match="year">
			description year="{description/year}"
		</xsl:template>

		<xsl:template match="author">
			description author="{description/author}"
		</xsl:template>
	</description>
	
	<sendingstatus>
		<xsl:value-of select="sendingstatus" />
	</sendingstatus>	

</xsl:stylesheet>