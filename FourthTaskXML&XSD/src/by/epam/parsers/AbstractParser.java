package by.epam.parsers;
import java.util.List;

import by.epam.entity.OldCard;
import by.epam.exceptions.LogicException;

public abstract class AbstractParser {
	public abstract List<OldCard> getOldCardsList(String url) throws LogicException;
	

}
