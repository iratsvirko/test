package by.epam.parsers;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.epam.entity.OldCard;
import by.epam.exceptions.LogicException;

public class DOMParser extends AbstractParser {

	@Override
	public List<OldCard> getOldCardsList(String url) throws LogicException {

		DocumentBuilder documentBuilder;
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new LogicException("parser configuration exception.", e);
		}
		Document document;
		try {
			document = documentBuilder.parse(new File(url));
		} catch (SAXException e) {
			throw new LogicException("sax exception.", e);
		} catch (IOException e) {
			throw new LogicException("io exception.", e);
		}

		List<OldCard> cards = null;
		if (document != null) {
			cards = doParsing(document);
		} else {
			throw new LogicException("document was not created.");
		}
		return cards;
	}

	private List<OldCard> doParsing(Document document) {
		List<OldCard> cardsList = new ArrayList<>();
		Element cards = document.getDocumentElement();
		NodeList cardsNodes = cards.getChildNodes();
		for (int i = 0; i < cardsNodes.getLength(); i++) {
			Node cardNode = cardsNodes.item(i);
			if (cardNode instanceof Element) {
				Element cardElement = (Element) cardNode;
				OldCard card = createOldCard(cardElement);
				cardsList.add(card);
				System.out.println(cardsList.toString());
			}
		}
		return cardsList;

	}

	private OldCard createOldCard(Element cardElement) {
		OldCard card = new OldCard();
		NodeList cardNodes = cardElement.getChildNodes();
		for (int i = 0; i < cardNodes.getLength(); i++) {
			Node cardNode = cardNodes.item(i);
			if (cardNode instanceof Element) {
				Element cardField = (Element) cardNode;
				
				if (isBarcode(cardField)) {
					card.setBarcode(cardField.getTextContent());
				}
				if (isTheme(cardField)) {
					card.setTheme(cardField.getTextContent());
				}
				if (isType(cardField)) {
					card.setType(cardField.getTextContent());
				}
				if (isTheme(cardField)) {
					card.setTheme(cardField.getTextContent());
				}
				if (isCountry(cardField)) {
					card.setCountry(cardField.getTextContent());
				}
				if (isYear(cardField)) {
					card.setYear(Integer.parseInt(cardField.getTextContent()));
				}
				if (isAuthor(cardField)) {
					card.setAuthor(cardField.getTextContent());
				}
				if (isValuable(cardField)) {
					card.setValuable(cardField.getTextContent());
				}
				if (isSendSt(cardField)) {
					card.setSendst(Boolean.parseBoolean(cardField.getTextContent()));
				}
			}
		}
		return card;
	}

	private boolean isBarcode(Element card) {
		return "barcode".equals(card.getTagName());
	}
	private boolean isTheme(Element card) {
		return "theme".equals(card.getTagName());
	}

	private boolean isType(Element card) {
		return "type".equals(card.getTagName());
	}

	private boolean isCountry(Element card) {
		return "country".equals(card.getTagName());
	}

	private boolean isYear(Element card) {
		return "year".equals(card.getTagName());
	}
	private boolean isAuthor(Element card) {
		return "author".equals(card.getTagName());
	}

	private boolean isValuable(Element card) {
		return "valuable".equals(card.getTagName());
	}
	private boolean isSendSt(Element card) {
		return "sendingstatus".equals(card.getTagName());
	}

}
