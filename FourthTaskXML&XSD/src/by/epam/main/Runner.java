package by.epam.main;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import by.epam.entity.OldCard;
import by.epam.exceptions.LogicException;
import by.epam.parsers.SaxHandler;
import by.epam.parsers.SaxParser;

public class Runner {

	public static void main(String[] args) {
		SaxParser first = new SaxParser();
		try {
			first.getOldCardsList("OldCards.xml");
		} catch (LogicException e) {
			e.printStackTrace();
		}

}}
