package by.epam.entity;

public class OldCard {
	private String barcode;
	private String theme;
	private String type;
	private String country;
	private int year;
	private boolean sendst;
	
	private String author;
	private String valuable;
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getValuable() {
		return valuable;
	}
	public void setValuable(String valuable) {
		this.valuable = valuable;
	}
	public boolean isSendst() {
		return sendst;
	}
	public void setSendst(boolean sendst) {
		this.sendst = sendst;
	}
	@Override
	public String toString() {
		return "OldCard [barcode=" + barcode + ", theme=" + theme + ", type=" + type + ", country=" + country
				+ ", year=" + year + ", sendst=" + sendst + ", author=" + author + ", valuable=" + valuable + "]";
	}

}
